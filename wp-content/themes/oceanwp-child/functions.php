<?php
/**
 * Child theme functions
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * Text Domain: oceanwp
 * @link http://codex.wordpress.org/Plugin_API
 *
 */

use Agents_Backend\TwilioService;

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Update jQuery to 3.3.1
 */
function sof_load_scripts() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js', '3.3.1', false );
		wp_enqueue_script( 'jquery' );
	}
}
add_action( 'wp_enqueue_scripts', 'sof_load_scripts' );

/**
 * Load the parent style.css file
 *
 * @link http://codex.wordpress.org/Child_Themes
 */
function oceanwp_child_enqueue_parent_style() {
	// Dynamically get version number of the parent stylesheet (lets browsers re-cache your stylesheet when you update your theme)
	$theme   = wp_get_theme( 'OceanWP' );
	$version = $theme->get( 'Version' );
	$version = time();

    // Load the Bootstrap Grid stylesheet
    wp_register_style( 'bootstrap-style', get_stylesheet_directory_uri() . '/assets/css/bootstrap-grid.min.css', false, '4.3.1' );
	wp_enqueue_style( 'bootstrap-style' );

    // Animate.css
    wp_enqueue_style( 'animate-css', get_stylesheet_directory_uri() . '/assets/css/animate.css', false, $version );

    // Load the stylesheet
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'oceanwp-style' ), $version );

    // Modules
    wp_enqueue_style( 'email-center-css', get_stylesheet_directory_uri() . '/assets/css/email-center.css', false, $version );
    wp_enqueue_style( 'agent-profile-css', get_stylesheet_directory_uri() . '/assets/css/agent-profile.css', false, $version );
    wp_enqueue_style( 'customer-request-form-css', get_stylesheet_directory_uri() . '/assets/css/customer-request-form.css', false, $version );
    wp_enqueue_style( 'agent-account-css', get_stylesheet_directory_uri() . '/assets/css/agent-account.css', false, $version );
    wp_enqueue_style( 'agents-applications-css', get_stylesheet_directory_uri() . '/assets/css/agents-applications.css', false, $version );
    wp_enqueue_style( 'agents-search-tool-css', get_stylesheet_directory_uri() . '/assets/css/agents-search-tool.css', false, $version );
    wp_enqueue_style( 'admin-customer-request-css', get_stylesheet_directory_uri() . '/assets/css/admin-customer-request.css', false, $version );

    //Scripts
    wp_enqueue_script('custom-scripts', get_stylesheet_directory_uri() . '/assets/js/custom-scripts.js', array('jquery'), time(), true );

    if ( is_page(334) ) {
        wp_enqueue_script('customer-scripts', get_stylesheet_directory_uri() . '/assets/js/customer-request.js', array('jquery'), time(), false );
        wp_localize_script( 'customer-scripts', 'ajax_customer_request', array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'ajax_nonce' => wp_create_nonce('customer_form')
        ) );
    }

	wp_enqueue_style(
		'akamaiagent-style',
		get_stylesheet_directory_uri() . '/dist/style.css',
		array(),
		_S_VERSION
	);

//	wp_enqueue_script(
//		'akamaiagent-scripts',
//		get_stylesheet_directory_uri() . '/dist/bundle.js',
//		array( 'jquery' ),
//		_S_VERSION,
//		true
//	);

}
add_action( 'wp_enqueue_scripts', 'oceanwp_child_enqueue_parent_style' );

// Setup theme => add_theme_support, register_nav_menus, load_theme_textdomain, etc
function custom_theme_setup(){
    add_image_size( 'agent-photo', 250, 400, true );
}
add_action( 'after_setup_theme', 'custom_theme_setup' );


// Add confirm email address validation
function custom_email_confirmation_validation_filter( $result, $tag ) {
    if ( 'agent-confirm-email' == $tag->name ) {
        $your_email = isset( $_POST['agent-email'] ) ? trim( $_POST['agent-email'] ) : '';
        $your_email_confirm = isset( $_POST['agent-confirm-email'] ) ? trim( $_POST['agent-confirm-email'] ) : '';
 
        if ( $your_email != $your_email_confirm ) {
            $result->invalidate( $tag, "Are you sure this is the correct address?" );
        }
    }
 
    return $result;
}
add_filter( 'wpcf7_validate_email*', 'custom_email_confirmation_validation_filter', 20, 2 );

// Add select label
function change_state_select_placeholder($html) {
	$text = 'State';
	$matches = false;
	preg_match('/<select name="agent-company-address-state"[^>]*>(.*)<\/select>/iU', $html, $matches);
	if ($matches) {
		$select = str_replace('<option value="">---</option>', '<option value="" disabled selected>' . $text . '</option>', $matches[0]);
		$html = preg_replace('/<select name="agent-company-address-state"[^>]*>(.*)<\/select>/iU', $select, $html);
	}
	return $html;
}
add_filter('wpcf7_form_elements', 'change_state_select_placeholder');