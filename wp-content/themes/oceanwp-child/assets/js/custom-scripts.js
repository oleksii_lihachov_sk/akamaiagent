(function($){

	"use strict";

	document.addEventListener( 'wpcf7mailsent', function( event ) {
	    setTimeout(function(){
			$('div.wpcf7 .fields-group > label, div.wpcf7 .width-50, div.wpcf7 .width-100, div.wpcf7 .fields-group > div:not(.wpcf7-response-output), div.wpcf7 .fields-group > p').attr('style', 'display: none !important');
			$('.wpcf7-response-output').addClass('alert alert-danger').html('Thank you for submitting your request. We have sent you an email confirmation. You may now return to the <a href="/">Home</a> page');
			$("html, body").animate({ scrollTop: 0 }, "fast");
		}, 100)
	}, false );

	$(document).ready(function() {
		
		$('div[data-url]').click(function(event) {
			window.location.href = $(this).data('url');
		});

	});


	var maskOptions = {
	  mask: '(000) 000-0000'
	};
	$('input[type="tel"]').each(function(index, el) {
		const element = $(this);
		IMask(element[0], maskOptions);
	});
	
})(jQuery)