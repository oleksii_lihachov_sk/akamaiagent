<?php

function agents_backend_get_languages() {
	$languages = array( 'Afrikaans', 'Albanian', 'Arabic', 'Armenian', 'Basque', 'Bengali', 'Bulgarian', 'Cantonese', 'Catalan', 'Cambodian', 'Chinese (Mandarin)', 'Croatian', 'Czech', 'Danish', 'Dutch', 'Estonian', 'Fiji', 'Finnish', 'French', 'Georgian', 'German', 'Greek', 'Gujarati', 'Hebrew', 'Hindi', 'Hungarian', 'Icelandic', 'Indonesian', 'Irish', 'Italian', 'Japanese', 'Javanese', 'Korean', 'Latin', 'Latvian', 'Lithuanian', 'Macedonian', 'Malay', 'Malayalam', 'Maltese', 'Maori', 'Marathi', 'Mongolian', 'Nepali', 'Norwegian', 'Persian', 'Polish', 'Portuguese', 'Punjabi', 'Quechua', 'Romanian', 'Russian', 'Samoan', 'Serbian', 'Slovak', 'Slovenian', 'Spanish', 'Swahili', 'Swedish', 'Tagalog', 'Tamil', 'Tatar', 'Telugu', 'Thai', 'Tibetan', 'Tonga', 'Turkish', 'Ukrainian', 'Urdu', 'Uzbek', 'Vietnamese', 'Welsh', 'Xhosa' );

	return apply_filters( 'agents_backend_languages', $languages );
}

function get_all_recipients() {
	$agents = get_users(
		array(
			'number'     => -1,
			'role'       => 'agent',
			'meta_key'   => 'agent_subscription',
			'meta_value' => true,
		)
	);

	$requests = get_posts(
		array(
			'post_type'   => 'request',
			'numberposts' => -1,
		)
	);

	$result   = array();

	foreach ( $agents as $key => $agent ) {
		$email  = strtolower( strval( $agent->user_email ) );

		if ( array_key_exists( $email, $result ) ) {
			continue;
		}

		$result[ $email ] = $agent->first_name . ' ' . $agent->last_name;
	}

	foreach ( $requests as $key => $request ) {
		$request_id = $request->ID;
		$email      = strtolower( strval( get_field( 'customer_request_customer_email', $request_id ) ) );

		if ( array_key_exists( $email, $result ) ) {
			continue;
		}

		$query = new \WP_Query(
			array(
				'post_type'      => array( 'customers' ),
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'   => '_customer_email',
						'value' => $email,
					),
				),
			)
		);

		if ( $query->posts ) {
			$post_id = $query->posts[0]->ID;
			$is_sub  = get_post_meta( $post_id, '_customer_subcription', true );

			if ( ! $is_sub ) {
				continue;
			}
		}

		$result[ $email ] = get_field( 'customer_request_customer_first_name', $request_id ) . ' ' . get_field( 'customer_request_customer_last_name', $request_id );
	}

	ksort( $result );

	return $result;
}

function get_all_recipients_detailed() {
	$agents = get_users(
		array(
			'number' => -1,
			'role'   => 'agent',
			'meta_key'   => 'agent_subscription',
			'meta_value' => true,
		)
	);

	$requests = get_posts(
		array(
			'post_type'   => 'request',
			'numberposts' => -1,
		)
	);
	$result   = array();

	foreach ( $agents as $key => $agent ) {
		$email  = strtolower( strval( $agent->user_email ) );

		if ( array_key_exists( $email, $result ) ) {
			continue;
		}

		$result[ $email ] = array(
			'first_name' => $agent->first_name,
			'last_name'  => $agent->last_name,
		);
	}

	foreach ( $requests as $key => $request ) {
		$request_id = $request->ID;
		$email      = strtolower( strval( get_field( 'customer_request_customer_email', $request_id ) ) );

		if ( array_key_exists( $email, $result ) ) {
			continue;
		}

		$query = new \WP_Query(
			array(
				'post_type'      => array( 'customers' ),
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'   => '_customer_email',
						'value' => $email,
					),
				),
			)
		);

		if ( $query->posts ) {
			$post_id = $query->posts[0]->ID;
			$is_sub  = get_post_meta( $post_id, '_customer_subcription', true );

			if ( ! $is_sub ) {
				continue;
			}
		}

		$result[ $email ] = array(
			'first_name' => get_field( 'customer_request_customer_first_name', $request_id ),
			'last_name'  => get_field( 'customer_request_customer_last_name', $request_id ),
		);
	}

	ksort( $result );

	return $result;
}

function get_customer_recipients_detailed() {
	$requests = get_posts(
		array(
			'post_type'   => 'request',
			'numberposts' => -1,
		)
	);

	$result = array();

	foreach ( $requests as $key => $request ) {
		$request_id     = $request->ID;
		$customer_email = strtolower( strval( get_field( 'customer_request_customer_email', $request_id ) ) );

		if ( array_key_exists( $customer_email, $result ) ) {
			continue;
		}

		$query = new \WP_Query(
			array(
				'post_type'      => array( 'customers' ),
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'   => '_customer_email',
						'value' => $customer_email,
					),
				),
			)
		);

		if ( $query->posts ) {
			$post_id = $query->posts[0]->ID;
			$is_sub  = get_post_meta( $post_id, '_customer_subcription', true );

			if ( ! $is_sub ) {
				continue;
			}
		}

		$result[ $customer_email ] = array(
			'first_name' => get_field( 'customer_request_customer_first_name', $request_id ),
			'last_name'  => get_field( 'customer_request_customer_last_name', $request_id ),
		);
	}

	ksort( $result );

	return $result;
}

function get_post_last_id( $post_type ) {
	$post = get_posts(
		array(
			'numberposts' => 1,
			'post_type'   => $post_type,
			'orderby'     => 'date',
			'order'       => 'DESC',
		)
	);

	switch ( $post_type ) {
		case 'agent_application':
			$id = get_field( 'application_id', $post[0]->ID );
			break;
		case 'request':
			$id     = get_field( 'customer_request_id', $post[0]->ID );
			$id_arr = explode( '-', $id );
			if ( is_array( $id_arr ) ) {
				$id = $id_arr[0];
			}
			break;

		default:
			$id = 0;
			break;
	}

	return $id;
}
