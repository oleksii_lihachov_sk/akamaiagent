<?php

namespace Agents_Backend;

/**
 * The class containing informatin about the plugin.
 */
class Agent {

	private $plugin_slug;
	private $version;
	private $available_preferences = array(
		'single_family'   => 'Single Family',
		'condo_townhouse' => 'Condo/Townhouse',
		'vacant_land'     => 'Vacant Land',
	);

	// Agent fields
	public $agent_id;
	public $status;
	public $vacation_status;
	public $first_name;
	public $last_name;
	public $email;
	public $language;
	public $company_name;
	public $zipcodes;

	public $seller_preferences;
	public $seller_property_value;
	public $buyer_preferences;
	public $buyer_property_value;

	public $socials;
	private $yelp_id;

	public $is_terms_accepted;

	public function __construct( $agent_id = null, $plugin_slug = '', $version = '' ) {

		if ( $agent_id ) {
			$this->set_fields( $agent_id );
		}

		$this->plugin_slug = $plugin_slug;
		$this->version     = $version;
	}


	private function set_fields( $agent_id ) {

		$user_info = get_userdata( $agent_id );

		$this->agent_id        = $agent_id;
		$this->status          = get_field( 'agent_status', 'user_' . $agent_id );
		$this->vacation_status = get_field( 'agent_vacation_status', 'user_' . $agent_id );
		$this->first_name      = get_field( 'agent_first_name', 'user_' . $agent_id );
		$this->last_name       = get_field( 'agent_last_name', 'user_' . $agent_id );
		$this->email           = $user_info->user_email;

		$languages = get_field( 'agent_speaks_languages', 'user_' . $agent_id );
		if ( is_array( $languages ) ) {
			$this->language = implode( ', ', get_field( 'agent_speaks_languages', 'user_' . $agent_id ) );
		} else {
			$this->language = $languages;
		}

		$this->company_name = get_field( 'agent_company_name', 'user_' . $agent_id );
		$this->zipcodes     = get_field( 'agent_zip', 'user_' . $agent_id );

		$this->seller_preferences    = get_field( 'agent_seller_preferences', 'user_' . $agent_id );
		$this->seller_property_value = get_field( 'agent_seller_property_value', 'user_' . $agent_id );
		$this->buyer_preferences     = get_field( 'agent_buyer_preferences', 'user_' . $agent_id );
		$this->buyer_property_value  = get_field( 'agent_buyer_property_value', 'user_' . $agent_id );

		$this->socials = get_field( 'agent_socials', 'user_' . $agent_id );

		$this->yelp_id = get_field( 'agent_ratings_link_yelp', 'user_' . $agent_id );

		$this->is_terms_accepted = ( get_field( 'agent_accepted_terms', 'user_' . $agent_id ) ) ? get_field( 'agent_accepted_terms', 'user_' . $agent_id ) : 'No';
	}


	/**
	 * Register Agent custom post type
	 *
	 * @return void
	 */
	public function register_agent_application_cpt() {

		$labels = array(
			'name'               => _x( 'Agent Applications', 'post type general name', $this->plugin_slug ),
			'singular_name'      => _x( 'Agent Application', 'post type singular name', $this->plugin_slug ),
			'menu_name'          => _x( 'Agent Applications', 'admin menu', $this->plugin_slug ),
			'name_admin_bar'     => _x( 'Agent Application', 'add new on admin bar', $this->plugin_slug ),
			'add_new'            => _x( 'Add Application', 'application', $this->plugin_slug ),
			'add_new_item'       => __( 'Add New Application', $this->plugin_slug ),
			'new_item'           => __( 'New Application', $this->plugin_slug ),
			'edit_item'          => __( 'Edit Application', $this->plugin_slug ),
			'view_item'          => __( 'View Application', $this->plugin_slug ),
			'all_items'          => __( 'All Applications', $this->plugin_slug ),
			'search_items'       => __( 'Search Agent Applications', $this->plugin_slug ),
			'parent_item_colon'  => __( 'Parent Applications:', $this->plugin_slug ),
			'not_found'          => __( 'No application found.', $this->plugin_slug ),
			'not_found_in_trash' => __( 'No application found in Trash.', $this->plugin_slug ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Agent custom poost type.', $this->plugin_slug ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'agent_application' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => 20,
			'supports'           => array( 'title', 'thumbnail' ),
			'menu_icon'          => 'dashicons-id-alt',
		);

		register_post_type( 'agent_application', $args );

	}

	public function validate_application_email( $result, $tag ) {
		$type = $tag['type'];
		$name = $tag['name'];

		if ( 'agent-email' === $name ) {
			$email = sanitize_email( $_POST[ $name ] );

			if ( email_exists( $email ) ) {
				$result->invalidate( $tag, 'Emails address is exists' );
			}
		}

		return $result;
	}

	/**
	 * Fill agent user id if agent email exist.
	 *
	 * @param  mixed $value value.
	 * @param  mixed $post_id id.
	 * @param  mixed $field data about field.
	 * @param  mixed $original value.
	 */
	public function filter_acf_update( $value, $post_id, $field, $original ) {
		if ( 'agent_user' === $field['name'] ) {
			if ( ! empty( $value ) ) {
				return $value;
			}

			$agent_email = get_field( 'agent_email', $post_id );

			if ( ! $agent_email || empty( $agent_email ) ) {
				return $value;
			}

			$user = get_user_by( 'email', $agent_email );

			if ( ! $user ) {
				// Generate random password.
				$random_password = wp_generate_password();
				$user_name       = $agent_email;

				// Insert new WP_User.
				$user_id = wp_create_user( $user_name, $random_password, $agent_email );

				// Update WP_User.
				$userdata = array(
					'ID'           => $user_id,
					'first_name'   => get_field( 'agent_first_name', $post_id ),
					'last_name'    => get_field( 'agent_last_name', $post_id ),
					'display_name' => get_field( 'agent_first_name', $post_id ) . ' ' . get_field( 'agent_last_name', $post_id ),
					'role'         => 'agent',
				);

				$user_id = wp_update_user( $userdata );

				Email::send_email(
					'agent-profile-registered',
					array(
						'to'         => $agent_email,
						'user_id'    => $user_id,
						'first_name' => get_field(
							'agent_first_name',
							$post_id
						),
					)
				);

				return $user_id;
			} else {
				return $user->ID;
			}
		}

		return $value;
	}


	/**
	 * Save Agent application to database
	 *
	 * @param object $cf7 Contact Form 7 object
	 *
	 * @return object  Modified Contact Form 7 object
	 */
	public function save_application( $cf7 ) {

		// get the contact form object
		$form_id = $cf7->id();

		// if you wanna check the ID of the Form $wpcf->id

		if ( $form_id == 13 ) {

			$last_application_id = get_post_last_id( 'agent_application' );
			if ( ! $last_application_id ) {
				$last_application_id = 0;
			}

			$post_data = array(
				'post_title'  => sanitize_text_field( $_POST['agent-first-name'] ) . ' ' . sanitize_text_field( $_POST['agent-last-name'] ),
				'post_type'   => 'agent_application',
				'post_status' => 'publish',
			);

			$agent_id = wp_insert_post( $post_data );

			update_field( 'application_id', $last_application_id + 1, $agent_id );
			update_field( 'agent_first_name', sanitize_text_field( $_POST['agent-first-name'] ), $agent_id );
			update_field( 'agent_last_name', sanitize_text_field( $_POST['agent-last-name'] ), $agent_id );
			update_field( 'agent_license_number', sanitize_text_field( $_POST['agent-license'] ), $agent_id );
			update_field( 'agent_email', sanitize_text_field( $_POST['agent-email'] ), $agent_id );
			update_field( 'agent_phone_office', sanitize_text_field( $_POST['agent-phone-office'] ), $agent_id );
			update_field( 'agent_phone_mobile', sanitize_text_field( $_POST['agent-phone-mobile'] ), $agent_id );

			// Company
			update_field( 'agent_company_name', sanitize_text_field( $_POST['agent-company-name'] ), $agent_id );
			update_field( 'agent_company_phone', sanitize_text_field( $_POST['agent-company-phone'] ), $agent_id );
			// Company Address
			update_field( 'agent_company_address_street', sanitize_text_field( $_POST['agent-company-address-street'] ), $agent_id );
			update_field( 'agent_company_address_suite_apartment', sanitize_text_field( $_POST['agent-company-address-suite'] ), $agent_id );
			update_field( 'agent_company_address_city', sanitize_text_field( $_POST['agent-company-address-city'] ), $agent_id );
			update_field( 'agent_company_address_state', sanitize_text_field( $_POST['agent-company-address-state'] ), $agent_id );
			update_field( 'agent_company_address_zip_code', sanitize_text_field( $_POST['agent-company-address-zip'] ), $agent_id );

			// Broker
			update_field( 'agent_broker_name', sanitize_text_field( $_POST['agent-broker-name'] ), $agent_id );
			update_field( 'agent_broker_email', sanitize_text_field( $_POST['agent-broker-email'] ), $agent_id );

			// Experience
			update_field( 'agent_valid_license', $_POST['agent_valid_license'], $agent_id );
			update_field( 'agent_licensed_3_years', $_POST['agent_licensed_3_years'], $agent_id );
			update_field( 'agent_nar_member', $_POST['agent_nar_member'], $agent_id );
			update_field( 'agent_10_transactiions', $_POST['agent_10_transactiions'], $agent_id );
			update_field( 'agent_4_stars_rating', $_POST['agent_4_stars_rating'], $agent_id );
			update_field( 'agent_ratings_link_yelp', sanitize_text_field( $_POST['agent_ratings_link_yelp'] ), $agent_id );
			update_field( 'agent_ratings_link_zillow', sanitize_text_field( $_POST['agent_ratings_link_zillow'] ), $agent_id );
			update_field( 'agent_comments', sanitize_text_field( $_POST['agent_questions'] ), $agent_id );

			// Application Status
			update_field( 'application_status', 'New', $agent_id );

			// Emails Data
			$email_data = array(
				'Application ID'                        => $last_application_id + 1,
				'Agent Name'                            => sanitize_text_field( $_POST['agent-first-name'] ) . ' ' . sanitize_text_field( $_POST['agent-last-name'] ),
				'Agent License Number'                  => sanitize_text_field( $_POST['agent-license'] ),
				'Agent Email'                           => sanitize_text_field( $_POST['agent-email'] ),
				'Agent Phone (Office)'                  => sanitize_text_field( $_POST['agent-phone-office'] ),
				'Agent Phone (Mobile)'                  => sanitize_text_field( $_POST['agent-phone-mobile'] ),

				// Company
				'Agent Company Name'                    => sanitize_text_field( $_POST['agent-company-name'] ),
				'Agent Company Phone'                   => sanitize_text_field( $_POST['agent-company-phone'] ),

				// Address
				'Company Address'                       => sanitize_text_field( $_POST['agent-company-address-street'] ) . ', ' . sanitize_text_field( $_POST['agent-company-address-suite'] ) . ', ' . sanitize_text_field( $_POST['agent-company-address-city'] ) . ', ' . sanitize_text_field( $_POST['agent-company-address-state'] ) . ', ' . sanitize_text_field( $_POST['agent-company-address-zip'] ),

				// Broker
				'Name & Email of your Broker in Charge (Enter “Me” if you are the BIC)' => sanitize_text_field( $_POST['agent-broker-name'] ) . ' (' . sanitize_text_field( $_POST['agent-broker-email'] ) . ')',

				// Experience
				'I maintain a valid Hawaii real estate license in good standing.' => ( isset( $_POST['agent_valid_license'] ) ) ? 'yes' : 'no',
				'I have been a licensed Hawaii real estate agent for a minimum of three (3) years.' => ( isset( $_POST['agent_licensed_3_years'] ) ) ? 'yes' : 'no',
				'I have closed a minimum of 10 transactions (sides) during the last 12 months.' => ( isset( $_POST['agent_10_transactiions'] ) ) ? 'yes' : 'no',
				'I maintain a minimum 4 star rating on Yelp or Zillow with a minimum of 4 reviews.' => ( isset( $_POST['agent_4_stars_rating'] ) ) ? 'yes' : 'no',
				'Enter preferred Ratings page (Yelp)'   => sanitize_text_field( $_POST['agent_ratings_link_yelp'] ),
				'Enter preferred Ratings page (Zillow)' => sanitize_text_field( $_POST['agent_ratings_link_zillow'] ),
				'I am a member of the National Association of Realtors (NAR)' => ( isset( $_POST['agent_nar_member'] ) ) ? 'yes' : 'no',
				'Have any questions or comments? We’d love to hear from you!' => sanitize_text_field( $_POST['agent_questions'] ),
			);

			// Email to applicant
			Email::send_email(
				'agent-new-application',
				array(
					'to'         => $_POST['agent-email'],
					'first_name' => $_POST['agent-first-name'],
					'last_name'  => $_POST['agent-last-name'],
				),
				$email_data
			);

			// Email to admin
			// Send email to admin
			Email::send_admin_email(
				'admin-new-agent-application',
				$email_data,
				array(
					'application_url' => home_url( '/admin-agents-applications/' ) . '?action=view&application_id=' . $agent_id,
				)
			);

			// Send Text Message
			$twilio = new TwilioService();
			$twilio->send_sms( 'agent_request', home_url( '/admin-agents-applications/' ) . '?action=view&application_id=' . $agent_id );

			$cf7->skip_mail = true;
		}

		return $cf7;
	}

	/**
	 * Approve application
	 *
	 * @param int $id Application ID
	 *
	 * @return string
	 */
	public static function approve_application( $application_id ) {

		if ( ! current_user_can( 'manage_options' ) ) {
			return 'Permission Denied';
		}

		if ( ! get_field( 'agent_email', $application_id ) || empty( get_field( 'agent_email', $application_id ) ) ) {
			return 'Email is not valid';
		}

		if ( email_exists( get_field( 'agent_email', $application_id ) ) ) {
			return 'Email is exists';
		}

		if ( 'Approved' === get_field( 'application_status', $application_id ) ) {
			return 'Application already approved';
		}

		// Application Status
		update_field( 'application_status', 'Approved', $application_id );

		Email::send_email(
			'agent-profile-approved',
			array(
				'to'         => get_field( 'agent_email', $application_id ),
				'first_name' => get_field(
					'agent_first_name',
					$application_id
				),
			)
		);

		return 'Application has been approved';
	}


	/**
	 * Reject application
	 *
	 * @param int $id Application ID
	 *
	 * @return string
	 */
	public static function reject_application( $application_id ) {

		if ( ! current_user_can( 'manage_options' ) ) {
			return 'Permission Denied';
		}

		if ( ! get_field( 'agent_email', $application_id ) || empty( get_field( 'agent_email', $application_id ) ) ) {
			update_field( 'application_status', 'Rejected', $application_id );
			return 'Email is not valid';
		}

		// if ( email_exists( get_field( 'agent_email', $application_id ) ) ) {
		// update_field( 'application_status', 'Rejected', $application_id );
		// return 'Email is exists';
		// }

		if ( 'Rejected' === get_field( 'application_status', $application_id ) ) {
			return 'Application already rejected';
		}

		// Application Status
		update_field( 'application_status', 'Rejected', $application_id );

		Email::send_email(
			'agent-profile-rejected',
			array(
				'to'         => get_field( 'agent_email', $application_id ),
				'first_name' => get_field(
					'agent_first_name',
					$application_id
				),
			)
		);

		return 'Application has been rejected';
	}


	/**
	 * Register WP User profile from application
	 *
	 * @param $application_id
	 *
	 * @return bool|string
	 */
	public static function register_profile( $application_id ) {

		if ( ! current_user_can( 'manage_options' ) ) {
			return 'Permission Denied';
		}

		if ( ! get_field( 'agent_email', $application_id ) || empty( get_field( 'agent_email', $application_id ) ) ) {
			return 'Email is not valid';
		}

		if ( email_exists( get_field( 'agent_email', $application_id ) ) ) {
			return 'Email is exists';
		}

		if ( 'Registered' === get_field( 'application_status', $application_id ) ) {
			return false;
		}

		// Generate random password
		$random_password = wp_generate_password( $length = 12, $include_standard_special_chars = false );

		$user_email = get_field( 'agent_email', $application_id );
		$user_name  = $user_email;

		// Insert new WP_User
		$user_id = wp_create_user( $user_name, $random_password, $user_email );

		// Update WP_User
		$userdata = array(
			'ID'           => $user_id,
			'first_name'   => get_field( 'agent_first_name', $application_id ),
			'last_name'    => get_field( 'agent_last_name', $application_id ),
			'display_name' => get_field( 'agent_first_name', $application_id ) . ' ' . get_field( 'agent_last_name', $application_id ),
			'role'         => 'agent',
		);
		$user_id  = wp_update_user( $userdata );

		// Update application
		update_field( 'application_status', 'Registered', $application_id );
		update_field( 'agent_user', $user_id, $application_id );

		// Add data from application to WP_User
		// General Info
		update_field( 'agent_first_name', get_field( 'agent_first_name', $application_id ), 'user_' . $user_id );
		update_field( 'agent_last_name', get_field( 'agent_last_name', $application_id ), 'user_' . $user_id );

		update_field( '_validate_email', time(), 'user_' . $user_id );
		update_field( 'agent_status', 'inactive', 'user_' . $user_id );
		update_field( 'agent_subscription', true, 'user_' . $user_id );
		update_field( 'agent_license_number', get_field( 'agent_license_number', $application_id ), 'user_' . $user_id );
		update_field( 'agent_phone_office', get_field( 'agent_phone_office', $application_id ), 'user_' . $user_id );
		update_field( 'agent_phone_mobile', get_field( 'agent_phone_mobile', $application_id ), 'user_' . $user_id );

		// Company
		update_field( 'agent_company_name', get_field( 'agent_company_name', $application_id ), 'user_' . $user_id );
		update_field( 'agent_company_phone', get_field( 'agent_company_phone', $application_id ), 'user_' . $user_id );
		// Company Address
		update_field( 'agent_company_address_street', get_field( 'agent_company_address_street', $application_id ), 'user_' . $user_id );
		update_field( 'agent_company_address_suite_apartment', get_field( 'agent_company_address_suite_apartment', $application_id ), 'user_' . $user_id );
		update_field( 'agent_company_address_city', get_field( 'agent_company_address_city', $application_id ), 'user_' . $user_id );
		update_field( 'agent_company_address_state', get_field( 'agent_company_address_state', $application_id ), 'user_' . $user_id );
		update_field( 'agent_company_address_zip_code', get_field( 'agent_company_address_zip_code', $application_id ), 'user_' . $user_id );

		// Experience
		update_field( 'agent_valid_license', get_field( 'agent_valid_license', $application_id ), 'user_' . $user_id );
		update_field( 'agent_licensed_3_years', get_field( 'agent_licensed_3_years', $application_id ), 'user_' . $user_id );
		update_field( 'agent_10_transactiions', get_field( 'agent_10_transactiions', $application_id ), 'user_' . $user_id );
		update_field( 'agent_4_stars_rating', get_field( 'agent_4_stars_rating', $application_id ), 'user_' . $user_id );
		update_field( 'agent_nar_member', get_field( 'agent_nar_member', $application_id ), 'user_' . $user_id );

		update_field( 'agent_ratings_link_yelp', get_field( 'agent_ratings_link_yelp', $application_id ), 'user_' . $user_id );
		update_field( 'agent_ratings_link_zillow', get_field( 'agent_ratings_link_zillow', $application_id ), 'user_' . $user_id );

		Email::send_email(
			'agent-profile-registered',
			array(
				'to'         => $user_email,
				'user_id'    => $user_id,
				'first_name' => get_field(
					'agent_first_name',
					$application_id
				),
			)
		);
	}

	/**
	 * Accept terms by Agent. AJAX callback
	 */
	public function save_edit_agent_profile_form( $post_id ) {

		if ( $post_id != 'user_' . get_current_user_id() ) {
			return $post_id;
		}

		if ( ! isset( $_POST['acf'] ) ) {
			return $post_id;
		}

		$first_name = ( isset( $_POST['acf']['field_5cc9b80293c11'] ) ) ? $_POST['acf']['field_5cc9b80293c11'] : '';
		$last_name  = ( isset( $_POST['acf']['field_5cc9b81493c12'] ) ) ? $_POST['acf']['field_5cc9b81493c12'] : '';

		// update other user info
		wp_update_user(
			array(
				'ID'           => get_current_user_id(),
				'first_name'   => $first_name,
				'last_name'    => $last_name,
				'display_name' => $first_name . ' ' . $last_name,
			)
		);

		$status        = 'inactive';
		$profile_check = $this->is_required_data_filled( get_current_user_id() );
		if ( $profile_check['result'] ) {
			$status = 'active';

			$agent_id    = get_current_user_id();
			$agent       = get_user_by( 'ID', $agent_id );
			$agent_email = $agent->user_email;

			if ( 'sent' !== get_field( 'agent_profile_completed_email', 'user_' . $agent_id ) && ! is_admin() ) {
				Email::send_email(
					'agent-profile-completed',
					array(
						'to'         => $agent_email,
						'first_name' => $first_name,
					)
				);

				update_field( 'agent_profile_completed_email', 'sent', 'user_' . $agent_id );
			}
		}

		update_field( 'agent_status', $status, $post_id );

		return $post_id;
	}

	public function update_agent_account() {
		$error       = array();
		$user_origin = get_user_by( 'id', $this->agent_id );

		/* Update user password. */
		if ( ! empty( $_POST['pass1'] ) && ! empty( $_POST['pass2'] ) ) {

			// Update password.
			if ( $_POST['pass1'] == $_POST['pass2'] ) {
				wp_update_user(
					array(
						'ID'        => $this->agent_id,
						'user_pass' => esc_attr( $_POST['pass1'] ),
					)
				);
				wp_password_change_notification( $user_origin );
			} else {
				$error[] = __( 'The passwords you entered do not match.  Your password was not updated.', 'agents-backend' );
			}
		}

		/* Update user information. */
		if ( ! empty( $_POST['email'] ) ) {
			if ( $user_origin->user_email !== $_POST['email'] ) {
				if ( ! is_email( esc_attr( $_POST['email'] ) ) ) {
								$error[] = __( 'The Email you entered is not valid.  please try again.', 'agents-backend' );
				} elseif ( email_exists( esc_attr( $_POST['email'] ) ) ) {
					$error[] = __( 'This email is already used by another user.  try a different one.', 'agents-backend' );
				} else {
					wp_update_user(
						array(
							'ID'         => $this->agent_id,
							'user_email' => esc_attr( $_POST['email'] ),
						)
					);
				}
			}
		} else {
			$error[] = __( 'The email is required.', 'agents-backend' );
		}

		// Update First name
		if ( ! empty( $_POST['first-name'] ) ) {
			update_user_meta( $this->agent_id, 'first_name', esc_attr( $_POST['first-name'] ) );
			update_field( 'agent_first_name', esc_attr( $_POST['first-name'] ), 'user_' . $this->agent_id );
		} else {
			$error[] = __( 'The first name is required.', 'agents-backend' );
		}

		// Update Last name
		if ( ! empty( $_POST['last-name'] ) ) {
			update_user_meta( $this->agent_id, 'last_name', esc_attr( $_POST['last-name'] ) );
			update_field( 'agent_last_name', esc_attr( $_POST['last-name'] ), 'user_' . $this->agent_id );
		} else {
			$error[] = __( 'The last name is required.', 'agents-backend' );
		}

		// Update Vacation status
		if ( isset( $_POST['vacation-status'] ) && $this->vacation_status !== esc_attr( $_POST['vacation-status'] ) ) {
			update_field( 'agent_vacation_status', esc_attr( $_POST['vacation-status'] ), 'user_' . $this->agent_id );

			Email::send_email(
				'agent-vacation-status-changed',
				array(
					'to'              => $this->email,
					'first_name'      => $this->first_name,
					'vacation_status' => esc_attr( $_POST['vacation-status'] ),
				)
			);

		}

		/* Redirect so the page will show updated info.*/
		if ( count( $error ) == 0 ) {
			// action hook for plugins and extra fields saving
			do_action( 'edit_user_profile_update', $this->agent_id );
			return $error;
		}

		return $error;
	}

	public function delete_agent_account() {

		check_ajax_referer( 'agents_nonce', 'nonce' );

		$agent_id = intval( $_POST['agent_id'] );

		if ( get_current_user_id() === $agent_id ) {
			wp_delete_user( $agent_id );
			wp_logout();
		} elseif ( current_user_can( 'manage_options' ) ) {
			wp_delete_user( $agent_id );
		} else {
			wp_send_json_error( '', 403 );
		}

		wp_send_json_success();
	}

	public static function get_agents( $params = false ) {

		$meta_query = array(
			'relation' => 'AND',
		);

		// Query by seller preferences
		$seller_preferences = ( isset( $_GET['agent_seller_preferences'] ) ) ? $_GET['agent_seller_preferences'] : false;
		if ( $seller_preferences ) {

			$meta_query[] = array(
				'key'     => 'agent_seller_preferences',
				'value'   => $_GET['agent_seller_preferences'],
				'compare' => 'LIKE',
			);
		}

		// Query by buyer preferences
		$buyer_preferences = ( isset( $_GET['agent_buyer_preferences'] ) ) ? $_GET['agent_buyer_preferences'] : false;
		if ( $buyer_preferences ) {
			$meta_query[] = array(
				'key'     => 'agent_buyer_preferences',
				'value'   => $buyer_preferences,
				'compare' => 'LIKE',
			);
		}

		// Query by property value
		$property_value = ( isset( $_GET['agent_property_value'] ) ) ? $_GET['agent_property_value'] : false;
		if ( $property_value ) {

			$agent_property_value = array(
				'relation' => 'OR',
			);

			$agent_property_value[] = array(
				'key'     => 'agent_seller_property_value',
				'value'   => $property_value,
				'compare' => 'LIKE',
			);
			$agent_property_value[] = array(
				'key'     => 'agent_buyer_property_value',
				'value'   => $property_value,
				'compare' => 'LIKE',
			);

			$meta_query[] = $agent_property_value;
		}

		// Query by language
		$need_language = ( isset( $_GET['language'] ) ) ? $_GET['language'] : false;
		if ( $need_language !== 'No' ) {
			$meta_query[] = array(
				'key'     => 'agent_speaks_languages',
				'value'   => $need_language,
				'compare' => 'LIKE',
			);
		}

		// Query by zip
		$zip = ( isset( $_GET['zipcode'] ) ) ? $_GET['zipcode'] : false;
		if ( $zip ) {
			$meta_query[] = array(
				'key'     => 'agent_zip',
				'value'   => $zip,
				'compare' => 'LIKE',
			);
		}

		// Search by name, email and company
		$word = ( isset( $_GET['word'] ) ) ? $_GET['word'] : false;
		if ( $word ) {

			// Find users by email
			$wp_user_query  = new \WP_User_Query(
				array(
					'search'         => "*{$word}*",
					'role'           => 'agent',
					'fields'         => array( 'ID' ),
					'search_columns' => array(
						'user_login',
						'user_nicename',
						'user_email',
					),
				)
			);
			$users_by_email = $wp_user_query->get_results();

			$s_meta_array = array(
				'relation' => 'OR',
				array(
					'key'     => 'display_name',
					'value'   => $word,
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'agent_company_name',
					'value'   => $word,
					'compare' => 'LIKE',
				),
					// array(
					// 'key'     => 'user_email',
					// 'value'   => $word,
					// 'compare' => 'LIKE'
					// ),
			);

			// Get each word of search request
			$s_parts = explode( ' ', $word );

			// // Prepare each word of search request as first and last names
			if ( ! empty( $s_parts ) ) {

				foreach ( $s_parts as $part ) {
					$s_meta_array[] = array(
						'key'     => 'first_name',
						'value'   => $part,
						'compare' => 'LIKE',
					);
					$s_meta_array[] = array(
						'key'     => 'last_name',
						'value'   => $part,
						'compare' => 'LIKE',
					);
				}
			}

			$meta_query[] = $s_meta_array;
		}

		// Pagination
		$paged        = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		$users_number = 50;

		if ( $paged === 1 ) {
			$offset = 0;
		} else {
			$offset = ( $paged - 1 ) * $users_number;
		}

		// Exclude founded by email agents
		$exclude = array();
		if ( isset( $users_by_email ) && is_array( $users_by_email ) && ! empty( $users_by_email ) ) {
			foreach ( $users_by_email as $key => $user ) {
				$exclude[] = $user->ID;
			}
		}

		// Params
		$only_fields = array( 'ID' );
		$args        = array(
			'role'       => 'agent',
			'number'     => $users_number,
			'fields'     => $only_fields,
			'offset'     => $offset,
			'meta_query' => $meta_query,
		);

		if ( ! empty( $exclude ) ) {
			$args['exclude'] = $exclude;
		}

		// Add aditional params
		if ( is_array( $params ) && ! empty( $params ) ) {
			$args = array_merge( $args, $params );
		}

		// Query
		$uq        = new \WP_User_Query( $args );
		$uq_result = $uq->get_results();

		if ( isset( $users_by_email ) && is_array( $users_by_email ) && ! empty( $users_by_email ) ) {
			$uq_result = array_merge( $uq_result, $users_by_email );
		}

		return $uq_result;
	}

	/**
	 * Accept terms by Agent. AJAX callback
	 */
	public function agent_accept_terms_callback() {

		$user_id = get_current_user_id();
		$result  = update_field( 'agent_accepted_terms', 'yes', 'user_' . $user_id );

		if ( $result ) {
			wp_send_json_success();
		}

		wp_send_json_error();

	}


	/**
	 * Get Yelp Account data
	 *
	 * @return string
	 */
	private function agent_yelp_rating( $id ) {

		if ( ! $id ) {
			return false;
		}

		$settings   = get_option( 'agents_backend_option' );
		$yelp_token = $settings['yelp_token'];

		$curl = curl_init();
		curl_setopt_array(
			$curl,
			array(
				CURLOPT_URL            => 'https://api.yelp.com/v3/businesses/' . $id,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => '',
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST  => 'GET',
				CURLOPT_POSTFIELDS     => '',
				CURLOPT_HTTPHEADER     => array(
					'Authorization: Bearer ' . $yelp_token,
				),
			)
		);

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		return json_decode( $response, true );

	}

	/**
	 * Render Yelp Agent rating
	 *
	 * @return string
	 */
	public function render_agent_yelp_rating() {

		$yelp_data = $this->agent_yelp_rating( $this->yelp_id );
		$error     = array();

		if ( isset( $yelp_data['error'] ) ) {
			$error[ $yelp_data['error']['code'] ] = $yelp_data['error']['description'];
			$agent_id                             = $this->agent_id;

			include_once plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-yelp-review.php';
			return;
		}

		switch ( $yelp_data['rating'] ) {
			case 5.0:
				$rating_class = 'i-stars--small-5';
				break;
			case 4.5:
				$rating_class = 'i-stars--small-4-half';
				break;
			case 4.0:
				$rating_class = 'i-stars--small-4';
				break;
			case 3.5:
				$rating_class = 'i-stars--small-3-half';
				break;
			case 3.0:
				$rating_class = 'i-stars--small-3';
				break;
			case 2.5:
				$rating_class = 'i-stars--small-2-half';
				break;
			case 2.0:
				$rating_class = 'i-stars--small-2';
				break;
			case 1.5:
				$rating_class = 'i-stars--small-1-half';
				break;
			case 1.0:
				$rating_class = 'i-stars--small-1';
				break;

			default:
				$rating_class = 'i-stars--small-0';
				break;
		}

		include_once plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-yelp-review.php';
	}


	/**
	 * Get Zillow agent rating
	 *
	 * @return string
	 */
	public static function agent_zillow_rating( $email ) {

		if ( ! $email ) {
			return false;
		}

		$settings       = get_option( 'agents_backend_option' );
		$zillow_api_key = $settings['zillow_api_key'];

		$curl = curl_init();
		curl_setopt_array(
			$curl,
			array(
				CURLOPT_URL            => 'http://www.zillow.com/webservice/ProReviews.htm?zws-id=' . $zillow_api_key . '&email=' . $email,
				// CURLOPT_URL => "http://www.zillow.com/webservice/ProReviews.htm?zws-id=" . $zillow_api_key . "&screenname=LewistownMTproperty",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING       => '',
				CURLOPT_MAXREDIRS      => 10,
				CURLOPT_TIMEOUT        => 30,
				CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST  => 'GET',
				CURLOPT_POSTFIELDS     => '',
			)
		);

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		$xml = simplexml_load_string( $response );
		return $xml->response->result;

	}

	/**
	 * Render Zillow Agent rating
	 *
	 * @return string
	 */
	public static function render_agent_zillow_rating( $email ) {

		$agent = new Agent();

		$zillow_data = $agent->agent_zillow_rating( $email );

		if ( ! $zillow_data ) {
			return false;
		}

		$rating       = (float) $zillow_data->proInfo->avgRating;
		$rating_class = 'zsg-rating_' . str_replace( '.', '', strval( number_format( $rating, 2, '.', '' ) ) );

		include_once plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-zillow-review.php';
	}


	/**
	 * Get agent detailed profile HTML to render on single request page
	 *
	 * @return string   Agent profile HTML
	 */
	public function get_datailed_profile_callback() {
		$agent_id = intval( $_POST['agent_id'] );
		$agent    = get_user_by( 'ID', $agent_id );

		ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/customer/customer-request-agent-profile.php';

		$html = ob_get_contents();
		ob_clean();

		echo $html;
		wp_die();
	}

	/**
	 * Contact agent after click to button in the profile.
	 *
	 * @return JSON   Contact email status
	 */
	public function contact_agent_callback() {

		$agent_id    = (int) $_POST['agent_id'];
		$agent       = get_user_by( 'ID', $agent_id );
		$agent_email = $agent->user_email;

		$agent_first_name = get_field( 'agent_first_name', 'user_' . $agent_id );
		$agent_last_name  = get_field( 'agent_last_name', 'user_' . $agent_id );

		$requeest_id        = (int) $_POST['customer_request_id'];
		$customer_name      = get_field( 'customer_request_customer_first_name', $requeest_id );
		$customer_last_name = get_field( 'customer_request_customer_last_name', $requeest_id );
		$customer_email     = get_field( 'customer_request_customer_email', $requeest_id );

		$message = wp_kses_post( stripslashes( $_POST['message'] ) );

		$params = array(
			'to'               => $agent_email,
			'agent_first_name' => $agent_first_name,
			'agent_last_name'  => $agent_last_name,
			'from'             => $customer_email,
			'from_name'        => $customer_name,
			'from_last_name'   => $customer_last_name,
			'message'          => $message,
		);

		$result = Email::send_email( 'agent-contact', $params );

		wp_send_json_success( $result );

		wp_die();
	}


	public function is_required_data_filled( $agent_id ) {

		$required_fields = array(
			'agent_photo'                     => 'Agent photo',
			'agent_first_name'                => 'Agent first name',
			'agent_last_name'                 => 'Agent last name',
			'agent_license_number'            => 'Agent license number',
			'agent_company_name'              => 'Company Name',
			'agent_company_phone'             => 'Agent Company Phone',
			'agent_company_address'           => array(
				'street'   => 'Company Street',
				// 'suite_apartment'=> 'Company Suite/Apartment',
				'city'     => 'Company City',
				'state'    => 'Company State',
				'zip_code' => 'Company Zip Code',
			),
			'agent_years_of_experience'       => 'Number of years as a licensed agent in the state of Hawaii',
			'agent_transactions_in_last_year' => 'Number of transactions (sides) in the last 12 months',
			'agent_ratings_link'              => array(
				'yelp'   => 'Yelp ID',
				'zillow' => 'Zillow',
			),
			'agent_about'                     => 'Aegnt Bio',
			'agent_zip'                       => 'At least one zip code is required',
			'agent_preferences'               => array(
				'agent_seller_preferences' => 'Seller preferences',
				'agent_buyer_preferences'  => 'Buyer preferences',
			),
		);

		$response = array(
			'result'   => true,
			'messages' => array(),
		);

		$fields = get_fields( 'user_' . $agent_id );

		foreach ( $required_fields as $key => $label ) {

			if ( is_array( $label ) ) {

				// Check sellers/buyers preferences
				if ( 'agent_preferences' === $key ) {
					if (
						( ! isset( $fields['agent_seller_preferences'] ) || ! $fields['agent_seller_preferences'] || empty( $fields['agent_seller_preferences'] ) ) &&
						( ! isset( $fields['agent_buyer_preferences'] ) || ! $fields['agent_buyer_preferences'] || empty( $fields['agent_buyer_preferences'] ) )
					) {
						$response['result'] = false;
						array_push( $response['messages'], 'Seller or Buyer preferences are required' );
					}

					if (
						( isset( $fields['agent_seller_preferences'] ) && $fields['agent_seller_preferences'] && ! empty( $fields['agent_seller_preferences'] ) ) &&
						( ! isset( $fields['agent_seller_property_value'] ) || ! $fields['agent_seller_property_value'] || empty( $fields['agent_seller_property_value'] ) )
					) {
						$response['result'] = false;
						array_push( $response['messages'], 'Seller property value is required' );
					}

					if (
						( isset( $fields['agent_buyer_preferences'] ) && $fields['agent_buyer_preferences'] && ! empty( $fields['agent_buyer_preferences'] ) ) &&
						( ! isset( $fields['agent_buyer_property_value'] ) || ! $fields['agent_buyer_property_value'] || empty( $fields['agent_buyer_property_value'] ) )
					) {
						$response['result'] = false;
						array_push( $response['messages'], 'Buyer property value is required' );
					}

					continue;

				}

				// Check rating pages
				if ( 'agent_ratings_link' === $key ) {
					if (
						( ! isset( $fields[ $key ]['yelp'] ) || ! $fields[ $key ]['yelp'] || empty( $fields[ $key ]['yelp'] ) ) &&
						( ! isset( $fields[ $key ]['zillow'] ) || ! $fields[ $key ]['zillow'] || empty( $fields[ $key ]['zillow'] ) )
					) {
						$response['result'] = false;
						array_push( $response['messages'], 'At least one of rating pages is required' );
					}

					continue;
				}

				foreach ( $label as $subkey => $label ) {

					if ( ! isset( $fields[ $key ][ $subkey ] ) || ! $fields[ $key ][ $subkey ] || empty( $fields[ $key ][ $subkey ] ) ) {
						$response['result'] = false;
						array_push( $response['messages'], $label . ' is required' );
					}
				}
			} else {
				if ( ! isset( $fields[ $key ] ) || ! $fields[ $key ] || empty( $fields[ $key ] ) ) {
					$response['result'] = false;
					array_push( $response['messages'], $label . ' is required' );
				}
			}
		}

		return $response;
	}

	public function render_preferences( $type = '' ) {
		$html = '';

		switch ( $type ) {
			case 'seller':
				$html .= '<strong>Seller</strong><br>';
				foreach ( $this->seller_preferences as $key => $value ) {
					$html .= '<span>' . $this->available_preferences[ $value['value'] ] . '</span><br>';
				}
				break;

			case 'buyer':
				$html .= '<strong>Buyer</strong><br>';
				foreach ( $this->buyer_preferences as $key => $value ) {
					$html .= '<span>' . $this->available_preferences[ $value['value'] ] . '</span><br>';
				}
				break;

			default:
				if ( is_array( $this->seller_preferences ) && ! empty( $this->seller_preferences ) ) {
					$html .= '<strong>Seller</strong><br>';
					foreach ( $this->seller_preferences as $key => $value ) {
						$html .= '<span>' . $this->available_preferences[ $value['value'] ] . '</span><br>';
					}
				}

				if ( is_array( $this->buyer_preferences ) && ! empty( $this->buyer_preferences ) ) {
					$html .= '<strong>Buyer</strong><br>';
					foreach ( $this->buyer_preferences as $key => $value ) {
						$html .= '<span>' . $this->available_preferences[ $value['value'] ] . '</span><br>';
					}
				}
				break;
		}

		return $html;
	}

	public function render_property_value( $type = '' ) {
		$html = '';

		switch ( $type ) {
			case 'seller':
				$html .= '<strong>Seller</strong><br>';
				foreach ( $this->seller_property_value as $key => $value ) {
					$html .= '<span>' . $value['label'] . '</span><br>';
				}
				break;

			case 'buyer':
				$html .= '<strong>Buyer</strong><br>';
				foreach ( $this->buyer_property_value as $key => $value ) {
					$html .= '<span>' . $value['label'] . '</span><br>';
				}
				break;

			default:
				if ( is_array( $this->seller_property_value ) && ! empty( $this->seller_property_value ) ) {
					$html .= '<strong>Seller</strong><br>';
					foreach ( $this->seller_property_value as $key => $value ) {
						$html .= '<span>' . $value['label'] . '</span><br>';
					}
				}

				if ( is_array( $this->buyer_property_value ) && ! empty( $this->buyer_property_value ) ) {
					$html .= '<strong>Buyer</strong><br>';
					foreach ( $this->buyer_property_value as $key => $value ) {
						$html .= '<span>' . $value['label'] . '</span><br>';
					}
				}
				break;
		}

		return $html;
	}

	public function update_user_select_query( $args, $field, $post ) {
		$args['meta_key']   = 'agent_status';
		$args['meta_value'] = 'active';
		return $args;
	}
}
