<?php
/**
 * Customers logic
 *
 *  @package null
 */

namespace Agents_Backend;

/**
 * Customer
 */
class Customer {
	/**
	 * Register Customer custom post type
	 *
	 * @return void
	 */
	public function register_cpt() {
		$labels = array(
			'name'               => _x( 'Customers', 'post type general name', 'agents-backend' ),
			'singular_name'      => _x( 'Customer', 'post type singular name', 'agents-backend' ),
			'menu_name'          => _x( 'Customers', 'admin menu', 'agents-backend' ),
			'name_admin_bar'     => _x( 'Customers', 'add new on admin bar', 'agents-backend' ),
			'add_new'            => _x( 'Add Customer', 'application', 'agents-backend' ),
			'add_new_item'       => __( 'Add Customer', 'agents-backend' ),
			'new_item'           => __( 'New Customer', 'agents-backend' ),
			'edit_item'          => __( 'Edit Customer', 'agents-backend' ),
			'view_item'          => __( 'View Customer', 'agents-backend' ),
			'all_items'          => __( 'All Customers', 'agents-backend' ),
			'search_items'       => __( 'Search Customer', 'agents-backend' ),
			'parent_item_colon'  => __( 'Parent Customer:', 'agents-backend' ),
			'not_found'          => __( 'No customers found.', 'agents-backend' ),
			'not_found_in_trash' => __( 'No customers found in Trash.', 'agents-backend' ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Agent custom poost type.', 'agents-backend' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'customers' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => 20,
			'supports'           => array( 'title' ),
			'menu_icon'          => 'dashicons-id-alt',
		);

		register_post_type( 'customers', $args );
	}

	/**
	 * Register Meta Boxes
	 *
	 * @return void
	 */
	public function add_box() {
		global $post;

		if ( 'customers' !== get_post_type( $post->ID ) ) {
			return;
		}

		add_meta_box(
			'customers_info',
			'Customers Info',
			array( $this, 'customers_form_html' ),
			null,
			'normal',
			'high'
		);

		add_meta_box(
			'customers_requests',
			'Requests',
			array( $this, 'customers_requests_html' ),
			null,
			'normal',
			'high'
		);
	}

	/**
	 * Post Content
	 *
	 * @param  mixed $post post object.
	 * @param  mixed $meta meta object.
	 * @return void
	 */
	public function customers_form_html( $post, $meta ) {
		$email  = get_post_meta( $post->ID, '_customer_email', true );
		$is_sub = get_post_meta( $post->ID, '_customer_subcription', true );

		wp_nonce_field( 'customer_form_nonce', 'customer_form_nonce' );

		// @codingStandardsIgnoreLine
		echo $this->form_styles();
		?>
		<div class="content">
			<div class="form-group">
				<label class="label">Email</label>
				<input class="form-control"
					type="email"
					placeholder="example@gmail.com" 
					name="customer_email"
					required
					value="<?php echo esc_attr( $email ); ?>"
				/>
			</div>
			<div class="form-group">
				<label class="label">Mail Subscription</label>
				<input class="form-control"
					type="checkbox"
					name="customer_subcription"
					value="1"
					<?php checked( $is_sub ); ?>
				/>
			</div>	
		</div>
		<?php
	}

	/**
	 * Post Content
	 *
	 * @param  mixed $post post object.
	 * @param  mixed $meta meta object.
	 * @return void
	 */
	public function customers_requests_html( $post, $meta ) {
		$result         = array();
		$customer_email = get_post_meta( $post->ID, '_customer_email', true );

		if ( empty( $customer_email ) ) {
			return;
		}

		$query = new \WP_Query(
			array(
				'post_type'      => array( 'request' ),
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'   => 'customer_request_customer_email',
						'value' => $customer_email,
					),
				),
			)
		);

		if ( empty( $query->posts ) ) {
			return;
		}

		foreach ( $query->posts as $request ) {
			$result[] = array(
				'title' => $request->post_title,
				'url'   => get_edit_post_link( $request->ID ),
			);
		}

		?>
		<table>
			<?php
			foreach ( $result as $item ) {
				?>
				<tr>
					<td><?php echo esc_attr( $item['title'] ); ?></td>
					<td><a href="<?php echo esc_url( $item['url'] ); ?>" target="_blank">Link</a></td>
				</tr>
				<?php
			}
			?>
		</table>
		<?php
	}

	/**
	 * Saving meta data
	 *
	 * @param  mixed $post_id id.
	 * @return void
	 */
	public function save_data( $post_id ) {
		if ( ! isset( $_POST['customer_form_nonce'] )
		|| ! wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST['customer_form_nonce'] ) ), 'customer_form_nonce' ) ) {
			$this->notification_helper( 'Wrong notice', 'error' );
				return;
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
				return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		$old_email = get_post_meta( $post_id, '_customer_email', true );
		$email     = isset( $_POST['customer_email'] ) ? sanitize_text_field( wp_unslash( $_POST['customer_email'] ) ) : '';
		$is_sub    = isset( $_POST['customer_subcription'] ) ? sanitize_text_field( wp_unslash( $_POST['customer_subcription'] ) ) : '';

		if ( $email && $email !== $old_email ) {
			if ( email_exists( $email ) ) {
				$this->notification_helper( 'Email is exist', 'error' );
				return;
			}

			$query = new \WP_Query(
				array(
					'post_type'      => array( 'customers' ),
					'posts_per_page' => -1,
					'meta_query'     => array(
						array(
							'key'   => '_customer_email',
							'value' => $email,
						),
					),
				)
			);

			if ( $query->posts ) {
				$this->notification_helper( 'Customer is exist', 'error' );
				return;
			}

			update_post_meta( $post_id, '_customer_email', $email );
		}

		update_post_meta( $post_id, '_customer_subcription', $is_sub );
	}

	/**
	 * Use this to add messages to the notifications heap during execution of your code
	 *
	 * @param  string $message message.
	 * @param  string $type type.
	 */
	public function notification_helper( $message, $type ) {
		if ( ! is_admin() ) {
			return false;
		}

		if ( ! in_array( $type, array( 'error', 'info', 'success', 'warning' ) ) ) {
			return false;
		}

		// Store/retrieve a transient associated with the current logged in user.
		$transient_name = 'admin_custom_notification_' . get_current_user_id();

		// Check if this transient already exists. We can use this to add.
		// multiple notifications during a single pass through our code.
		$notifications = get_transient( $transient_name );

		if ( ! $notifications ) {
			$notifications = array(); // initialise as a blank array.
		}

		$notifications[] = array(
			'message' => $message,
			'type'    => $type,
		);

		set_transient( $transient_name, $notifications ); // no need to provide an expiration, will be removed immediately.
	}

	/**
	 * The handler to output our admin notification messages
	 */
	public function custom_admin_notice_handler() {
		if ( ! is_admin() ) {
			// Only process this when in admin context.
			return;
		}

		$transient_name = 'admin_custom_notification_' . get_current_user_id();

		// Check if there are any notices stored.
		$notifications = get_transient( $transient_name );

		if ( $notifications ) :
			foreach ( $notifications as $notification ) :
				?>
				<div class="notice notice-custom notice-<?php echo esc_attr( $notification['type'] ); ?> is-dismissible">
					<p><?php echo esc_attr( $notification['message'] ); ?></p>
				</div>
				<?php
			endforeach;
		endif;

		// Clear away our transient data, it's not needed any more.
		delete_transient( $transient_name );
	}

	/**
	 * General styles
	 *
	 * @return string
	 */
	private function form_styles() {
		ob_start();
		?>
		<style type="text/css">
		.form-group {
			margin-bottom: 1rem;
		}

		.label {
			display: inline-block;
			margin-bottom: .5rem;
		}

		.form-control {
			display: block;
			width: 100%;
			padding: .375rem .75rem;
			font-size: 1rem;
			line-height: 1.5;
			color: #495057;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: .25rem;
			transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
		}

		.form-control:focus {
			color: #495057;
			background-color: #fff;
			border-color: #80bdff;
			outline: 0;
			box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, .25);
		}
		</style>
		<?php
		return ob_get_clean();
	}
}
