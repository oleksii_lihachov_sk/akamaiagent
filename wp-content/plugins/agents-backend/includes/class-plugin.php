<?php

namespace Agents_Backend;

/**
 * The main plugin class.
 */
class Plugin {
	private $loader;
	private $plugin_slug;
	private $version;
	private $option_name;

	public function __construct() {
		$this->plugin_slug = Info::SLUG;
		$this->version     = Info::VERSION;
		$this->option_name = Info::OPTION_NAME;
		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_frontend_hooks();
	}

	private function load_dependencies() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-loader.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-admin.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-agent.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-request.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-email.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/class-frontend.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/functions.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-twilio.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-user.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-customer.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) . '/vendor/autoload.php';

		$this->loader = new Loader();
	}

	private function define_admin_hooks() {
		$plugin_admin = new Admin( $this->plugin_slug, $this->version, $this->option_name );
		$agent        = new Agent( null, $this->plugin_slug, $this->version );
		$request      = new Request( null, $this->plugin_slug, $this->version );
		$email        = new Email( $this->plugin_slug, $this->version );
		$customer     = new Customer();

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'assets' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menus' );

		// Agents.
		$this->loader->add_action( 'init', $agent, 'register_agent_application_cpt' );

		// Customers.
		$this->loader->add_action( 'init', $customer, 'register_cpt' );
		$this->loader->add_action( 'add_meta_boxes', $customer, 'add_box' );
		$this->loader->add_action( 'save_post', $customer, 'save_data' );
		$this->loader->add_action( 'admin_notices', $customer, 'custom_admin_notice_handler' );

		// Requests.
		$this->loader->add_action( 'init', $request, 'register_customer_request_cpt' );
		$this->loader->add_filter( 'manage_request_posts_columns', $request, 'columns_request' );
		$this->loader->add_action( 'manage_request_posts_custom_column', $request, 'custom_columns_content', 10, 2 );

		// Emails.
		$this->loader->add_action( 'init', $email, 'register_email_template_cpt' );
		$this->loader->add_action( 'init', $email, 'register_system_email_template_cpt' );
		$this->loader->add_filter( 'retrieve_password_message', $email, 'custom_retrieve_password_message', 10, 4 );
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
		$this->loader->add_filter( 'wp_mail_content_type', $email, 'set_html_content_type' );
	}

	private function define_frontend_hooks() {
		$plugin_frontend = new Frontend( $this->plugin_slug, $this->version, $this->option_name );
		$agent           = new Agent( null, $this->plugin_slug, $this->version );
		$email           = new Email( $this->plugin_slug, $this->version );
		$request         = new Request( null, $this->plugin_slug, $this->version );
		$user            = new Agents_Backend_User();

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_frontend, 'assets' );
		$this->loader->add_action( 'wp_head', $plugin_frontend, 'add_acf_form_head', 1 );
		$this->loader->add_action( 'template_redirect', $plugin_frontend, 'users_redirects' );
		$this->loader->add_action( 'login_redirect', $plugin_frontend, 'login_redirect', 10, 3 );
		$this->loader->add_action( 'wp_footer', $plugin_frontend, 'show_popup_messages' );

		// Agents
		add_shortcode( 'agents_applicatiions', array( $plugin_frontend, 'render_agents_applications' ) );
		add_shortcode( 'agent_profile', array( $plugin_frontend, 'render_agent_profile' ) );
		add_shortcode( 'agent_account', array( $plugin_frontend, 'render_agent_account' ) );
		add_shortcode( 'agents_search_tool', array( $plugin_frontend, 'render_agents_search_tool' ) );
		$this->loader->add_filter( 'wpcf7_validate_email*', $agent, 'validate_application_email', 10, 2 );
		$this->loader->add_action( 'wpcf7_mail_sent', $agent, 'save_application', 20, 1 );
		$this->loader->add_action( 'acf/save_post', $agent, 'save_edit_agent_profile_form' );
		$this->loader->add_action( 'wp_ajax_agent_accept_terms', $agent, 'agent_accept_terms_callback' );
		$this->loader->add_action( 'wp_ajax_get_datailed_profile', $agent, 'get_datailed_profile_callback' );
		$this->loader->add_action( 'wp_ajax_contact_agent', $agent, 'contact_agent_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_contact_agent', $agent, 'contact_agent_callback' );
		$this->loader->add_action( 'wp_ajax_delete_agent', $agent, 'delete_agent_account' );
		$this->loader->add_filter( 'acf/update_value', $agent, 'filter_acf_update', 10, 4 );
		$this->loader->add_filter( 'acf/fields/user/query/name=customer_request_assigned_agents', $agent, 'update_user_select_query', 10, 3 );

		// Request
		add_shortcode( 'customer_requests', array( $plugin_frontend, 'render_customer_requests' ) );
		$this->loader->add_action( 'template_redirect', $request, 'maybe_confirm_customer_email_address' );

		// Ajax
		$this->loader->add_action( 'wp_ajax_save_customer_request', $request, 'save_customer_request_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_save_customer_request', $request, 'save_customer_request_callback' );
		$this->loader->add_action( 'wp_ajax_add_agent_to_request', $request, 'add_agent_to_request_callback' );
		$this->loader->add_action( 'wp_ajax_remove_agent_from_request', $request, 'remove_agent_from_request_callback' );
		$this->loader->add_action( 'wp_ajax_render_selected_agents', $request, 'render_selected_agents_callback' );
		$this->loader->add_action( 'wp_ajax_search_agents', $request, 'search_agents_callback' );
		$this->loader->add_action( 'wp_ajax_send_agents_to_customer', $request, 'send_agents_to_customer_callback' );
		$this->loader->add_action( 'wp_ajax_change_request_status', $request, 'change_request_status_callback' );

		// Email.
		$this->loader->add_action( 'wp_ajax_set_email_template', $email, 'set_email_template_callback' );
		$this->loader->add_filter( 'wp_password_change_notification_email', $email, 'override_password_change_email', 10, 3 );

		// Admin
		add_shortcode( 'admin_account', array( $plugin_frontend, 'render_admin_account' ) );
		add_shortcode( 'email_center', array( $plugin_frontend, 'render_email_center' ) );

		// Contact Us
		$this->loader->add_action( 'wpcf7_mail_sent', $plugin_frontend, 'contact_us_email_sent' );

		// Users
		$this->loader->add_filter( 'get_avatar', $user, 'profile_avatar', 10, 5 );
		$this->loader->add_action( 'wp_before_admin_bar_render', $user, 'admin_bar_render' );
	}

	public function run() {
		$this->loader->run();
	}
}
