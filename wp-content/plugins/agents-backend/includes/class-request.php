<?php

namespace Agents_Backend;

/**
 * The class containing informatin about the plugin.
 */
class Request {
	private $plugin_slug;
	private $version;
	public $available_statuses  = array(
		'pending'         => 'Pending',
		'active'          => 'Active',
		'is_agents_sent'  => 'Sent',
		'deactivated'     => 'Deactivated',
		'deleted'         => 'Deleted',
		'pending_payment' => 'Closed – Pending Payment',
		'paid'            => 'Closed – Paid',
	);
	private $available_zipcodes = array(
		96701,
		96703,
		96704,
		96705,
		96706,
		96707,
		96708,
		96709,
		96710,
		96712,
		96713,
		96714,
		96715,
		96716,
		96717,
		96718,
		96719,
		96720,
		96721,
		96722,
		96725,
		96726,
		96727,
		96728,
		96729,
		96730,
		96731,
		96732,
		96733,
		96734,
		96737,
		96738,
		96739,
		96740,
		96741,
		96742,
		96743,
		96744,
		96745,
		96746,
		96747,
		96748,
		96749,
		96750,
		96751,
		96752,
		96753,
		96754,
		96755,
		96756,
		96757,
		96759,
		96760,
		96761,
		96762,
		96763,
		96764,
		96765,
		96766,
		96767,
		96768,
		96769,
		96770,
		96771,
		96772,
		96773,
		96774,
		96776,
		96777,
		96778,
		96779,
		96780,
		96781,
		96782,
		96783,
		96784,
		96785,
		96786,
		96788,
		96789,
		96790,
		96791,
		96792,
		96793,
		96795,
		96796,
		96797,
		96801,
		96802,
		96803,
		96804,
		96805,
		96806,
		96807,
		96808,
		96809,
		96810,
		96811,
		96812,
		96813,
		96814,
		96815,
		96816,
		96817,
		96818,
		96819,
		96820,
		96821,
		96822,
		96823,
		96824,
		96825,
		96826,
		96827,
		96828,
		96830,
		96835,
		96836,
		96837,
		96838,
		96839,
		96840,
		96841,
		96843,
		96844,
		96846,
		96847,
		96848,
		96849,
		96850,
		96853,
		96854,
		96857,
		96858,
		96859,
		96860,
		96861,
		96863,
		96898,
	);

	public $request_id;
	public $request_custom_id;
	public $request_type;
	public $request_status;
	public $request_result;
	public $customer_email;
	public $customer_first_name;
	public $customer_last_name;

	public $seller = null;
	public $buyer  = null;

	public $zip = null;

	public $need_language;
	public $other_language = 'English';
	public $outside_of_hawaii;
	public $assigned_agents;
	public $is_email_confirmed;

	public function __construct( $request_id = null, $plugin_slug = '', $version = '' ) {

		$custom_request_id = $this->get_wp_id_from_custom( $request_id );

		if ( $custom_request_id ) {
			$request_id = $custom_request_id;
		}

		if ( $request_id ) {
			$this->set_fields( $request_id );
		}

		$this->plugin_slug = $plugin_slug;
		$this->version     = $version;
	}

	private function set_fields( $request_id ) {
		$this->request_id          = $request_id;
		$this->request_custom_id   = get_field( 'customer_request_id', $request_id );
		$this->request_type        = get_field( 'customer_request_customer_type', $request_id );
		$this->request_status      = get_field( 'customer_request_status', $request_id );
		$this->request_result      = get_field( 'customer_request_result', $request_id );
		$this->customer_email      = get_field( 'customer_request_customer_email', $request_id );
		$this->customer_first_name = get_field( 'customer_request_customer_first_name', $request_id );
		$this->customer_last_name  = get_field( 'customer_request_customer_last_name', $request_id );

		switch ( $this->request_type ) {
			case 'Seller':
				$this->seller = $this->get_request_seller_parameters();
				break;

			case 'Buyer':
				$this->buyer = $this->get_request_buyer_parameters();
				break;

			case 'Seller & Buyer':
				$this->seller = $this->get_request_seller_parameters();
				$this->buyer  = $this->get_request_buyer_parameters();
				break;

			default:
				break;
		}

		if ( 'Seller' === $this->request_type ) {
			$this->zip = $this->seller['address']['zipcode'];
		} else {
			$this->zip = $this->buyer['address']['zipcode'];
		}

		$this->need_language = get_field( 'customer_request_need_language', $request_id );

		if ( 'No' !== $this->need_language ) {
			$this->other_language = get_field( 'customer_request_other_language', $request_id );
		}

		$this->outside_of_hawaii  = get_field( 'customer_request_outside_of_hawaii', $request_id );
		$this->assigned_agents    = get_field( 'customer_request_assigned_agents', $request_id );
		$this->is_email_confirmed = get_field( 'customer_request_is_email_confirmed', $request_id );
	}

	/**
	 * Register Request custom post type
	 *
	 * @return void
	 */
	public function register_customer_request_cpt() {

		$labels = array(
			'name'               => _x( 'Requests', 'post type general name', $this->plugin_slug ),
			'singular_name'      => _x( 'Request', 'post type singular name', $this->plugin_slug ),
			'menu_name'          => _x( 'Customer Requests', 'admin menu', $this->plugin_slug ),
			'name_admin_bar'     => _x( 'Request', 'add new on admin bar', $this->plugin_slug ),
			'add_new'            => _x( 'Add New', 'request', $this->plugin_slug ),
			'add_new_item'       => __( 'Add New Request', $this->plugin_slug ),
			'new_item'           => __( 'New Request', $this->plugin_slug ),
			'edit_item'          => __( 'Edit Request', $this->plugin_slug ),
			'view_item'          => __( 'View Request', $this->plugin_slug ),
			'all_items'          => __( 'All Requests', $this->plugin_slug ),
			'search_items'       => __( 'Search Requests', $this->plugin_slug ),
			'parent_item_colon'  => __( 'Parent Requests:', $this->plugin_slug ),
			'not_found'          => __( 'No requests found.', $this->plugin_slug ),
			'not_found_in_trash' => __( 'No requests found in Trash.', $this->plugin_slug ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Request custom poost type.', $this->plugin_slug ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'request' ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => 21,
			'supports'           => array( 'title', 'thumbnail' ),
			'menu_icon'          => 'dashicons-search',
		);

		register_post_type( 'request', $args );

	}

	public function columns_request( $columns ) {

		unset( $columns['date'] );

		$columns['request_type']   = 'Type';
		$columns['parent_request'] = 'Parent Request';
		$columns['date']           = 'Date';

		return $columns;
	}

	public function custom_columns_content( $column, $post_id ) {
		switch ( $column ) {
			case 'request_type':
				echo get_field( 'customer_request_customer_type', $post_id );
				break;
			case 'parent_request':
				echo get_field( 'customer_request_parent_id', $post_id );
				break;
		}
	}

	/**
	 * Save Request from frontend form. AJAX callback
	 *
	 * @return void
	 */
	public function save_customer_request_callback() {

		check_ajax_referer( 'customer_form', 'nonce' );

		$type = $_POST['answers']['type'];

		if ( $type === 'Seller' || $type === 'Buyer' ) {
			$request_id = $this->save_single_request_object( $_POST );
		} elseif ( $type === 'Seller & Buyer' ) {
			$request_id_seller = $this->save_multiple_request_object( 'Seller', $_POST );
			$request_id        = $this->save_multiple_request_object( 'Buyer', $_POST );
			update_field( 'customer_request_parent_id', $request_id_seller, $request_id );
		}

		wp_send_json_success();

	}

	/**
	 * Save single request
	 *
	 * @param  array $data $_POST data
	 * @return [type]       [description]
	 */
	private function save_single_request_object( $data ) {
		// Customer Name.
		$first_name = sanitize_text_field( $data['answers']['General']['customer_request_customer_first_name'] );
		$last_name  = sanitize_text_field( $data['answers']['General']['customer_request_customer_last_name'] );
		$type       = $data['answers']['type'];

		// Get Last request ID.
		$last_request_id = get_post_last_id( 'request' );
		if ( ! $last_request_id ) {
			$last_request_id = 0;
		}

		// New Request data.
		$post_data = array(
			'post_title'  => '#' . $last_request_id,
			'post_type'   => 'request',
			'post_status' => 'publish',
		);

		// Create new Request
		$request_id = wp_insert_post( $post_data );

		// Update Request title. Add ID to title
		// Updated data
		$new_post_data = array(
			'ID'         => $request_id,
			'post_title' => '#' . $request_id . ' - ' . $first_name . ' ' . $last_name,
		);

		// Update Request
		wp_update_post( $new_post_data );

		// Update custom ID
		update_field( 'customer_request_id', $last_request_id + 1, $request_id );

		// Request Status
		update_field( 'customer_request_status', $this->available_statuses['pending'], $request_id );

		// Request result
		update_field( 'customer_request_result', sanitize_text_field( $data['answers']['result'] ), $request_id );

		// Request type
		update_field( 'customer_request_customer_type', sanitize_text_field( $data['answers']['type'] ), $request_id );

		// Customer Name
		update_field( 'customer_request_customer_first_name', $first_name, $request_id );
		update_field( 'customer_request_customer_last_name', $last_name, $request_id );

		// Customer Email
		update_field( 'customer_request_customer_email', sanitize_text_field( $data['answers']['General']['customer_request_customer_email'] ), $request_id );

		// Need Language?
		update_field( 'customer_request_need_language', sanitize_text_field( $data['answers']['General']['customer_request_need_language'] ), $request_id );

		// Other language
		if ( isset( $data['answers']['General']['customer_request_other_language'] ) && ! empty( $data['answers']['General']['customer_request_other_language'] && 'No' !== $data['answers']['General']['customer_request_need_language'] ) ) {
			update_field( 'customer_request_other_language', sanitize_text_field( $data['answers']['General']['customer_request_other_language'] ), $request_id );
		} else {
			update_field( 'customer_request_other_language', 'English', $request_id );
		}

		// Outside of Hawaii
		if ( isset( $data['answers']['General']['customer_request_outside_of_hawaii'] ) && ! empty( $data['answers']['General']['customer_request_outside_of_hawaii'] ) ) {
			update_field( 'customer_request_outside_of_hawaii', sanitize_text_field( $data['answers']['General']['customer_request_outside_of_hawaii'] ), $request_id );
		}

		// Save additional property fields
		// Save Seller or Buyer fields
		foreach ( $data['answers'][ $type ] as $key => $value ) {
			update_field( 'customer_request_' . strtolower( $type ) . '_' . $key, sanitize_text_field( $value ), $request_id );
		}

		// Update Request
		wp_update_post( array( 'ID' => $request_id ) );

		$request_class = new Request( $request_id );

		// Dont sent Emails if customer doesn't want
		if ( $data['answers']['result'] !== 'GN-6' ) {

			// Generate confirmation email token
			$confirmation_token = wp_generate_password( 32, false );
			update_field( 'customer_request_confirmation_email_token', $confirmation_token, $request_id );

			// Send email to customer
			Email::send_email(
				'customer-confirmation-email',
				array(
					'to'                 => $request_class->customer_email,
					'first_name'         => $request_class->customer_first_name,
					'confirmation_token' => $confirmation_token,
					'request_id'         => $request_id,
				)
			);
		}

		$query = new \WP_Query(
			array(
				'post_type'      => array( 'customers' ),
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'   => '_customer_email',
						'value' => sanitize_text_field( $data['answers']['General']['customer_request_customer_email'] ),
					),
				),
			)
		);

		// Add new customer.
		if ( empty( $query->posts ) ) {
			// New Customer data.
			$customer_post_data = array(
				'post_title'  => $first_name . ' - ' . $last_name,
				'post_type'   => 'customers',
				'post_status' => 'publish',
			);

			$customer_id = wp_insert_post( $customer_post_data );
			update_post_meta( $customer_id, '_customer_email', sanitize_text_field( $data['answers']['General']['customer_request_customer_email'] ) );
			update_post_meta( $customer_id, '_customer_subcription', true );
		}

		return $request_id;
	}


	private function save_multiple_request_object( $type, $data ) {

		// Customer Name
		$first_name = sanitize_text_field( $data['answers']['General']['customer_request_customer_first_name'] );
		$last_name  = sanitize_text_field( $data['answers']['General']['customer_request_customer_last_name'] );

		$last_request_id = get_post_last_id( 'request' );
		if ( ! $last_request_id ) {
			$last_request_id = 0;
		}

		// New Request data
		$post_data = array(
			'post_title'  => '#' . $last_request_id,
			'post_type'   => 'request',
			'post_status' => 'publish',
		);

		// Create new Request
		$request_id = wp_insert_post( $post_data );

		// Update Request title. Add ID to title
		// Updated data
		$new_post_data = array(
			'ID'         => $request_id,
			'post_title' => '#' . $request_id . ' - ' . $first_name . ' ' . $last_name,
		);

		// Update Request
		wp_update_post( $new_post_data );

		// Add Request data
		if ( $type === 'Buyer' ) {
			$custom_id = $last_request_id . '-' . $type[0];
		} else {
			$custom_id = ( $last_request_id + 1 ) . '-' . $type[0];
		}
		update_field( 'customer_request_id', $custom_id, $request_id );

		// Request Status
		update_field( 'customer_request_status', $this->available_statuses['pending'], $request_id );

		// Request result
		update_field( 'customer_request_result', sanitize_text_field( $data['answers']['result'] ), $request_id );

		// Request type
		update_field( 'customer_request_customer_type', $type, $request_id );

		// Customer Name
		update_field( 'customer_request_customer_first_name', sanitize_text_field( $data['answers']['General']['customer_request_customer_first_name'] ), $request_id );
		update_field( 'customer_request_customer_last_name', sanitize_text_field( $data['answers']['General']['customer_request_customer_last_name'] ), $request_id );

		// Customer Email
		update_field( 'customer_request_customer_email', sanitize_text_field( $data['answers']['General']['customer_request_customer_email'] ), $request_id );

		// Need Language?
		update_field( 'customer_request_need_language', sanitize_text_field( $data['answers']['General']['customer_request_need_language'] ), $request_id );

		// Other language
		if ( isset( $data['answers']['General']['customer_request_other_language'] ) && ! empty( $data['answers']['General']['customer_request_other_language'] && 'No' !== $data['answers']['General']['customer_request_need_language'] ) ) {
			update_field( 'customer_request_other_language', sanitize_text_field( $data['answers']['General']['customer_request_other_language'] ), $request_id );
		} else {
			update_field( 'customer_request_other_language', 'English', $request_id );
		}

		// Outside of Hawaii
		if ( isset( $data['answers']['General']['customer_request_outside_of_hawaii'] ) && ! empty( $data['answers']['General']['customer_request_outside_of_hawaii'] ) ) {
			update_field( 'customer_request_outside_of_hawaii', sanitize_text_field( $data['answers']['General']['customer_request_outside_of_hawaii'] ), $request_id );
		}

		// Save additional property fields
		// Save Seller or Buyer fields
		foreach ( $data['answers'][ $type ] as $key => $value ) {
			update_field( 'customer_request_' . strtolower( $type ) . '_' . $key, sanitize_text_field( $value ), $request_id );
		}

		// Update Request
		wp_update_post( array( 'ID' => $request_id ) );

		$request_class = new Request( $request_id );

		if ( $type === 'Buyer' ) {
			// Dont sent Emails if customer doesn't want
			if ( $data['answers']['result'] !== 'GN-6' ) {

				// Generate confirmation email token
				$confirmation_token = wp_generate_password( 32, false );
				update_field( 'customer_request_confirmation_email_token', $confirmation_token, $request_id );

				// Send email to customer
				Email::send_email(
					'customer-confirmation-email',
					array(
						'to'                 => $request_class->customer_email,
						'first_name'         => $request_class->customer_first_name,
						'confirmation_token' => $confirmation_token,
						'request_id'         => $request_id,
					)
				);
			}
		}

		$query = new \WP_Query(
			array(
				'post_type'      => array( 'customers' ),
				'posts_per_page' => -1,
				'meta_query'     => array(
					array(
						'key'   => '_customer_email',
						'value' => sanitize_text_field( $data['answers']['General']['customer_request_customer_email'] ),
					),
				),
			),
		);

		// Add new customer.
		if ( empty( $query->posts ) ) {
			// New Customer data.
			$customer_post_data = array(
				'post_title'  => $first_name . ' - ' . $last_name,
				'post_type'   => 'customers',
				'post_status' => 'publish',
			);

			$customer_id = wp_insert_post( $customer_post_data );
			update_post_meta( $customer_id, '_customer_email', sanitize_text_field( $data['answers']['General']['customer_request_customer_email'] ) );
			update_post_meta( $customer_id, '_customer_subcription', true );
		}

		return $request_id;

	}

	public function maybe_confirm_customer_email_address() {
		if ( ! isset( $_GET['action'] ) || $_GET['action'] !== 'confirm_email' ) {
			return false;
		}

		$request_id         = intval( $_GET['request_id'] );
		$confirmation_token = get_field( 'customer_request_confirmation_email_token', $request_id );

		if ( $confirmation_token !== $_GET['confirmation_token'] ) {
			wp_redirect( site_url() . '?action=email_confirmation_wrong' );
		}

		$request_class = new Request( $request_id );
		$request_class->customer_email_confirmation_process();

		// If request has parent ID, it means if custome wants sell and buy at same time
		$parent_request = get_field( 'customer_request_parent_id', $request_id );
		if ( $parent_request &&
			is_numeric( $parent_request ) ) {
			$parent_request_class = new Request( $parent_request );
			$parent_request_class->customer_email_confirmation_process();
		}

		wp_redirect( site_url() . '?action=email_confirmed' );
	}

	/**
	 * This function adds agents to requests, prepares and sends email to customer and admin
	 * with info that new customer has been registered.
	 *
	 * @return void
	 */
	private function customer_email_confirmation_process() {

		// Save best agents
		$best_agents = $this->get_best_agents_by_dinamic_zip();

		$this->assign_agents_to_new_request( $this->request_id, $best_agents );

		// Update confirmation status
		update_field( 'customer_request_is_email_confirmed', 'yes', $this->request_id );

		// Remove token
		update_field( 'customer_request_confirmation_email_token', '', $this->request_id );

		// Send email to customer
		Email::send_email(
			'customer-new-request',
			array(
				'to'         => $this->customer_email,
				'first_name' => $this->customer_first_name,
				'type'       => $this->request_type,
				'result'     => $this->request_result,
			),
			$this
		);

		// // Send email to admin
		Email::send_admin_email(
			'admin-new-customer-request',
			array(
				'first_name' => $this->customer_first_name,
				'type'       => $this->request_type,
				'request_id' => $this->request_id,
				'result'     => $this->request_result,
			),
			$this
		);

		// // Send Text Message
		$twilio = new TwilioService();
		$twilio->send_sms( 'customer_request', 'https://akamaiagent.com/admin-customers-requests/?action=view&request_id=' . $this->request_id );

	}


	/**
	 * Assign Agents to request
	 *
	 * @param  int   $request_id Customer reequest ID
	 * @param  array $agents     Array of WP_User objects
	 * @return void
	 */
	private function assign_agents_to_new_request( $request_id, $agents ) {

		$agents_ids = array();

		for ( $i = 0; $i < 3; $i++ ) {
			if ( isset( $agents[ $i ] ) ) {
				array_push( $agents_ids, $agents[ $i ]->ID );
			}
		}

		update_field( 'customer_request_assigned_agents', $agents_ids, $request_id );
	}


	/**
	 * Get best match agents for Request criteria
	 *
	 * @param  int $request_id Customer reequest ID
	 * @return array  Array of WP_User objects
	 */
	public static function get_customer_requests() {

		$meta_query = array(
			'relation' => 'AND',
		);

		// Search by customer name
		if ( isset( $_GET['customer-name'] ) && ! empty( $_GET['customer-name'] ) ) {

			$name = array(
				'relation' => 'OR',
			);

			$s_parts = explode( ' ', $_GET['customer-name'] );

			// Prepare each word of search request as first and last names
			if ( ! empty( $s_parts ) ) {

				foreach ( $s_parts as $part ) {
					$name[] = array(
						'key'     => 'customer_request_customer_first_name',
						'value'   => $part,
						'compare' => 'LIKE',
					);

					$name[] = array(
						'key'     => 'customer_request_customer_last_name',
						'value'   => $part,
						'compare' => 'LIKE',
					);
				}
			}

			$meta_query[] = $name;
		}

		// Request Stsatus
		if ( isset( $_GET['request-status'] ) && $_GET['request-status'] !== '' ) {

			$meta_query[] = array(
				'key'     => 'customer_request_status',
				'value'   => $_GET['request-status'],
				'compare' => 'LIKE',
			);
		}

		// Search by request ID
		if ( isset( $_GET['request-id'] ) && ! empty( $_GET['request-id'] ) ) {

			$meta_query[] = array(
				'key'     => 'customer_request_id',
				'value'   => $_GET['request-id'],
				'compare' => 'LIKE',
			);
		}

		// Query by customer email
		if ( isset( $_GET['customer-email'] ) && $_GET['customer-email'] !== '' ) {

			$meta_query[] = array(
				'key'     => 'customer_request_customer_email',
				'value'   => $_GET['customer-email'],
				'compare' => 'LIKE',
			);

		}

		// Query by customer type
		$customer_type = ( isset( $_GET['customer-type'] ) ) ? strtolower( $_GET['customer-type'] ) : '';
		if ( $customer_type !== '' ) {

			$meta_query[] = array(
				'key'     => 'customer_request_customer_type',
				'value'   => $customer_type,
				'compare' => 'LIKE',
			);

		}

		// Query by property preferences
		if ( isset( $_GET['property-type'] ) && $_GET['property-type'] !== '' ) {

			$property_type = array(
				'relation' => 'OR',
			);

			if ( $customer_type === 'seller' ) {
				$property_type[] = array(
					'key'     => 'customer_request_seller_customer_request_property_type',
					'value'   => $_GET['property-type'],
					'compare' => 'LIKE',
				);
			} elseif ( $customer_type === 'buyer' ) {
				$property_type[] = array(
					'key'     => 'customer_request_buyer_customer_request_property_type',
					'value'   => $_GET['property-type'],
					'compare' => 'LIKE',
				);
			} else {
				$property_type[] = array(
					'key'     => 'customer_request_seller_customer_request_property_type',
					'value'   => $_GET['property-type'],
					'compare' => 'LIKE',
				);

				$property_type[] = array(
					'key'     => 'customer_request_buyer_customer_request_property_type',
					'value'   => $_GET['property-type'],
					'compare' => 'LIKE',
				);
			}

			$meta_query[] = $property_type;
		}

		// Query by property price
		if ( isset( $_GET['property-price'] ) && $_GET['property-price'] !== '' ) {

			$property_type = array(
				'relation' => 'OR',
			);

			$property_type[] = array(
				'key'     => 'customer_request_seller_customer_request_price_range',
				'value'   => $_GET['property-price'],
				'compare' => 'LIKE',
			);

			$property_type[] = array(
				'key'     => 'customer_request_buyer_customer_request_price_range',
				'value'   => $_GET['property-price'],
				'compare' => 'LIKE',
			);

			$meta_query[] = $property_type;
		}

		// Query by zip
		if ( isset( $_GET['zipcode'] ) && $_GET['zipcode'] !== '' ) {
			$zipcode_arr = array(
				'relation' => 'OR',
			);

			$zipcode_arr[] = array(
				'key'     => 'customer_request_seller_customer_request_property_address_zipcode',
				'value'   => $_GET['zipcode'],
				'compare' => 'LIKE',
			);

			$zipcode_arr[] = array(
				'key'     => 'customer_request_buyer_customer_request_customer_zip',
				'value'   => $_GET['zipcode'],
				'compare' => 'LIKE',
			);

			$meta_query[] = $zipcode_arr;
		}

		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

		// Query args
		$args = array(
			'post_type'      => 'request',
			'paged'          => $paged,
			'posts_per_page' => 20,
			'meta_query'     => $meta_query,
		);

		// Search by date
		if ( isset( $_GET['request-date'] ) && ! empty( $_GET['request-date'] ) ) {

			$day = getdate( strtotime( $_GET['request-date'] ) );

			$args['year']     = $day['year'];
			$args['monthnum'] = $day['mon'];
			$args['day']      = $day['mday'];
		}

		// Get Posts request
		$requests = new \WP_Query( $args );

		return $requests;

	}


	/**
	 * Get best match agents for Request criteria
	 *
	 * @param  int $request_id Customer reequest ID
	 * @return array  Array of WP_User objects
	 */
	public static function get_best_match_agents( $request_id, $custom_zip = null, $number = 10 ) {

		$agent_preferences = array(
			'Single Family Home' => 'single_family',
			'Condo/Townhouse'    => 'condo_townhouse',
			'Vacant Land'        => 'vacant_land',
		);

		// Get assigned to request agents
		$selected_agents = get_field( 'customer_request_assigned_agents', $request_id );

		$meta_query = array(
			'relation' => 'AND',
			array(
				'key'     => 'agent_status',
				'value'   => 'active',
				'compare' => 'LIKE',
			),
			array(
				'relation' => 'OR',
				array(
					'key'     => 'agent_vacation_status',
					'value'   => 'no',
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'agent_vacation_status',
					'compare' => 'NOT EXISTS',
				),
			),
		);

		// Query by seller preferences
		$seller_property_type = get_field( 'customer_request_seller_customer_request_property_type', $request_id );
		if ( $seller_property_type ) {

			$meta_query[] = array(
				'key'     => 'agent_seller_preferences',
				'value'   => $agent_preferences[ $seller_property_type ],
				'compare' => 'LIKE',
			);
		}

		// Query by buyer preferences
		$buyer_property_type = get_field( 'customer_request_buyer_customer_request_property_type', $request_id );
		if ( $buyer_property_type ) {
			$meta_query[] = array(
				'key'     => 'agent_buyer_preferences',
				'value'   => $agent_preferences[ $buyer_property_type ],
				'compare' => 'LIKE',
			);
		}

		// Query by property value
		$seller_price = get_field( 'customer_request_seller_customer_request_price_range', $request_id );
		$buyer_price  = get_field( 'customer_request_buyer_customer_request_price_range', $request_id );
		if ( $seller_price || $buyer_price ) {

			$agent_property_value = array(
				'relation' => 'OR',
			);

			if ( $seller_price && ! $buyer_price ) {
				$agent_property_value[] = array(
					'key'     => 'agent_seller_property_value',
					'value'   => $seller_price,
					'compare' => 'LIKE',
				);
				$agent_property_value[] = array(
					'key'     => 'agent_buyer_property_value',
					'value'   => $seller_price,
					'compare' => 'LIKE',
				);
			} elseif ( ! $seller_price && $buyer_price ) {
				$agent_property_value[] = array(
					'key'     => 'agent_seller_property_value',
					'value'   => $buyer_price,
					'compare' => 'LIKE',
				);
				$agent_property_value[] = array(
					'key'     => 'agent_buyer_property_value',
					'value'   => $buyer_price,
					'compare' => 'LIKE',
				);
			} else {
				$agent_property_value[] = array(
					'key'     => 'agent_seller_property_value',
					'value'   => $seller_price,
					'compare' => 'LIKE',
				);
				$agent_property_value[] = array(
					'key'     => 'agent_buyer_property_value',
					'value'   => $buyer_price,
					'compare' => 'LIKE',
				);
			}

			$meta_query[] = $agent_property_value;
		}

		// Query by language
		$need_language = get_field( 'customer_request_need_language', $request_id );
		if ( $need_language !== 'No' ) {
			$meta_query[] = array(
				'key'     => 'agent_speaks_languages',
				'value'   => $need_language,
				'compare' => 'LIKE',
			);
		}

		// Query by zip
		if ( get_field( 'customer_request_seller_customer_request_property_address_zipcode', $request_id ) ) {
			if ( $custom_zip ) {
				$zip = $custom_zip;
			} else {
				$zip = get_field( 'customer_request_seller_customer_request_property_address_zipcode', $request_id );
			}

			$meta_query[] = array(
				'key'     => 'agent_zip',
				'value'   => $zip,
				'compare' => 'LIKE',
			);
		} elseif ( get_field( 'customer_request_buyer_customer_request_customer_zip', $request_id ) ) {
			if ( $custom_zip ) {
				$zip = $custom_zip;
			} else {
				$zip = get_field( 'customer_request_buyer_customer_request_customer_zip', $request_id );
			}

			$meta_query[] = array(
				'key'     => 'agent_zip',
				'value'   => $zip,
				'compare' => 'LIKE',
			);
		}

		$agents = get_users(
			array(
				'number'     => $number,
				'role'       => 'agent',
				'exclude'    => $selected_agents,
				'meta_query' => $meta_query,
			)
		);

		return $agents;
	}

	private function get_best_agents_by_dinamic_zip() {
		$best_agents = array();
		$zip_tmp     = $this->zip;
		$skip_start  = false;
		$index       = 1;

		while ( count( $best_agents ) < 3 && $index <= 5 ) {

			if ( ! $skip_start ) {
				$best_agents = array_merge( $best_agents, $this->get_best_match_agents( $this->request_id, $zip_tmp, ( 3 - count( $best_agents ) ) ) );
				$skip_start  = true;
			}

			if ( count( $best_agents ) < 3 && ( intval( $zip_tmp ) + $index ) <= 96898 ) {
				$best_agents = array_merge( $best_agents, $this->get_best_match_agents( $this->request_id, intval( $zip_tmp ) + $index, ( 3 - count( $best_agents ) ) ) );
			}

			if ( count( $best_agents ) < 3 && ( intval( $zip_tmp ) - $index ) >= 96701 ) {
				$best_agents = array_merge( $best_agents, $this->get_best_match_agents( $this->request_id, intval( $zip_tmp ) - $index, ( 3 - count( $best_agents ) ) ) );
			}

			$index++;
		}

		return $best_agents;
	}

	/**
	 * Assiign agent to Request. AJAX callback
	 *
	 * @return void
	 */
	public function add_agent_to_request_callback() {
		$agent_id   = intval( $_POST['agent_id'] );
		$request_id = intval( $_POST['request_id'] );

		// Check IDs of agent and request
		if ( ! is_numeric( $agent_id ) || ! is_numeric( $request_id ) ) {
			wp_send_json_error( 'Parameters are not valid', 400 );
		}

		// Get assigned to request agents
		$agents = get_field( 'customer_request_assigned_agents', $request_id );

		// Check if assigned agents less than 3
		if ( is_array( $agents ) && count( $agents ) >= 3 ) {
			wp_send_json_error( 'Request has 3 assigned agents', 400 );
		}

		// Update array with current agents
		if ( is_array( $agents ) ) {
			$agents[] = $agent_id;
		} else {
			$agents = array( $agent_id );
		}

		// Update field
		$result = update_field( 'customer_request_assigned_agents', $agents, $request_id );

		// Send updating result
		if ( $result ) {
			wp_send_json_success( 'Agent has been assigned to request', 200 );
		} else {
			wp_send_json_error( 'Something wrong', 400 );
		}

		wp_die();
	}


	/**
	 * Remove agent from Request. AJAX callback
	 *
	 * @return void
	 */
	public function remove_agent_from_request_callback() {
		$agent_id   = intval( $_POST['agent_id'] );
		$request_id = intval( $_POST['request_id'] );

		// Check IDs of agent and request
		if ( ! is_numeric( $agent_id ) || ! is_numeric( $request_id ) ) {
			wp_send_json_error( 'Parameters are not valid', 400 );
		}

		// Get assigned to request agents
		$agents = get_field( 'customer_request_assigned_agents', $request_id );

		if ( ! is_array( $agents ) ) {
			wp_send_json_error( 'There are not selected agents', 400 );
		}

		// Remove agent from agents array
		unset( $agents[ array_search( $agent_id, $agents ) ] );

		// Update field
		$result = update_field( 'customer_request_assigned_agents', $agents, $request_id );

		// Send updating result
		if ( $result ) {
			wp_send_json_success( 'Agent has been removed from request', 200 );
		} else {
			wp_send_json_error( 'Something wrong', 400 );
		}

		wp_die();
	}


	/**
	 * Assiign agent to Request. AJAX callback
	 *
	 * @return string
	 */
	public function render_selected_agents_callback() {

		$request_id = $_POST['request_id'];

		ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/customer/customer-request-selected-agents.php';

		$html = ob_get_contents();
		ob_clean();

		wp_send_json_success( $html );

		wp_die();
	}


	/**
	 * Search agents in the to Request. AJAX callback
	 *
	 * @return string
	 */
	public function search_agents_callback() {

		$request_id = $_POST['request_id'];
		$data       = $_POST['data'];

		// Get assigned to request agents
		$selected_agents = get_field( 'customer_request_assigned_agents', $request_id );

		$meta_query = array(
			'relation' => 'AND',
			array(
				'key'     => 'agent_status',
				'value'   => 'active',
				'compare' => 'LIKE',
			),
			array(
				'relation' => 'OR',
				array(
					'key'     => 'agent_vacation_status',
					'value'   => 'no',
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'agent_vacation_status',
					'compare' => 'NOT EXISTS',
				),
			),
		);

		// Query by seller preferences
		if ( isset( $data['agent_seller_preferences'] ) ) {
			$arr = explode( ',', $data['agent_seller_preferences'] );

			$agent_seller_preferences = array(
				'relation' => 'OR',
			);

			foreach ( $arr as $value ) {
				$agent_seller_preferences[] = array(
					'key'     => 'agent_seller_preferences',
					'value'   => $value,
					'compare' => 'LIKE',
				);
			}

			$meta_query[] = $agent_seller_preferences;
		}

		// Query by buyer preferences
		if ( isset( $data['agent_buyer_preferences'] ) ) {
			$arr = explode( ',', $data['agent_buyer_preferences'] );

			$agent_buyer_preferences = array(
				'relation' => 'OR',
			);

			foreach ( $arr as $value ) {
				$agent_buyer_preferences[] = array(
					'key'     => 'agent_buyer_preferences',
					'value'   => $value,
					'compare' => 'LIKE',
				);
			}

			$meta_query[] = $agent_buyer_preferences;
		}

		// Query by property value
		if ( isset( $data['agent_property_value'] ) ) {
			$arr = explode( ',', $data['agent_property_value'] );

			$agent_property_value = array(
				'relation' => 'OR',
			);

			foreach ( $arr as $value ) {
				$agent_property_value[] = array(
					'relation' => 'OR',
					array(
						'key'     => 'agent_seller_property_value',
						'value'   => $value,
						'compare' => 'LIKE',
					),
					array(
						'key'     => 'agent_buyer_property_value',
						'value'   => $value,
						'compare' => 'LIKE',
					),
				);
			}

			$meta_query[] = $agent_property_value;
		}

		// Query by language
		if ( isset( $data['language'] ) ) {
			$meta_query[] = array(
				'key'     => 'agent_speaks_languages',
				'value'   => $data['language'],
				'compare' => 'LIKE',
			);
		}

		// Query by zipcode
		if ( isset( $data['zipcode'] ) ) {
			$meta_query[] = array(
				'key'     => 'agent_zip',
				'value'   => $data['zipcode'],
				'compare' => 'LIKE',
			);
		}

		// Search by name, email and company
		if ( isset( $data['s'] ) && ! empty( $data['s'] ) ) {

			// Find users by email
			$word           = $data['s'];
			$wp_user_query  = new \WP_User_Query(
				array(
					'search'         => "*{$word}*",
					'role'           => 'agent',
					// 'exclude'    => $selected_agents,
					'fields'         => array( 'ID' ),
					'search_columns' => array(
						'user_login',
						'user_nicename',
						'user_email',
					),
				)
			);
			$users_by_email = $wp_user_query->get_results();

			$s_meta_array = array(
				'relation' => 'OR',
				array(
					'key'     => 'display_name',
					'value'   => $data['s'],
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'agent_company_name',
					'value'   => $data['s'],
					'compare' => 'LIKE',
				),
				array(
					'key'     => 'user_email',
					'value'   => $data['s'],
					'compare' => 'LIKE',
				),
			);

			// Get each word of search request
			$s_parts = explode( ' ', $data['s'] );

			// Prepare each word of search request as first and last names
			if ( ! empty( $s_parts ) ) {

				foreach ( $s_parts as $part ) {
					$s_meta_array[] = array(
						'key'     => 'first_name',
						'value'   => $part,
						'compare' => 'LIKE',
					);
					$s_meta_array[] = array(
						'key'     => 'last_name',
						'value'   => $part,
						'compare' => 'LIKE',
					);
				}
			}

			$meta_query[] = $s_meta_array;
		}

		// Prepare query
		$query = array(
			'number'     => 10,
			'role'       => 'agent',
			'meta_query' => $meta_query,
			'fields'     => array( 'ID' ),
		);

		if ( is_array( $selected_agents ) || isset( $users_by_email ) ) {
			// Exclude selected agents by email
			if ( is_array( $users_by_email ) && ! empty( $users_by_email ) ) {
				foreach ( $users_by_email as $key => $user ) {
					$selected_agents[] = $user->ID;
				}
			}
			$query['exclude'] = $selected_agents;
		}

		// Get agents
		// Query
		$uq     = new \WP_User_Query( $query );
		$agents = $uq->get_results();

		if ( isset( $users_by_email ) && is_array( $users_by_email ) && ! empty( $users_by_email ) ) {
			$agents = array_merge( $agents, $users_by_email );
		}

		ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/customer/customer-request-agents-table.php';

		$html = ob_get_contents();
		ob_clean();

		wp_send_json_success(
			array(
				'query' => $query,
				'html'  => $html,
			)
		);

		wp_die();
	}

	public function send_agents_to_customer_callback() {

		$request_id      = (int) $_POST['request_id'];
		$assigned_agents = get_field( 'customer_request_assigned_agents', $request_id );
		$request_class   = new Request( $request_id );

		// Send email to customer
		Email::send_email(
			'customer-assigned-agents',
			array(
				'to'                  => get_field( 'customer_request_customer_email', $request_id ),
				'first_name'          => get_field( 'customer_request_customer_first_name', $request_id ),
				'agents'              => $assigned_agents,
				'customer_request_id' => $request_id,
			),
			$request_class
		);

		foreach ( $assigned_agents as $agent_id ) {

			$user = get_user_by( 'ID', $agent_id );

			// Send email to agent
			Email::send_email(
				'agent-new-customer-referral',
				array(
					'to'                  => $user->user_email,
					'agent_first_name'    => $user->first_name,
					'agent_last_name'     => $user->last_name,
					'customer_request_id' => $request_id,
				),
				$request_class
			);
		}

		wp_send_json_success();

		wp_die();
	}

	public function get_request_seller_parameters() {

		$seller = array(
			'property_type' => $this->get_request_type_property_type( 'seller' ),
			'price_range'   => $this->get_request_price( 'seller' ),
			'how_soon'      => get_field( 'customer_request_seller_customer_request_how_soon', $this->request_id ),
			'address'       => array(
				'street'  => get_field( 'customer_request_seller_customer_request_property_address_street', $this->request_id ),
				'unit'    => get_field( 'customer_request_seller_customer_request_property_address_unit', $this->request_id ),
				'city'    => get_field( 'customer_request_seller_customer_request_property_address_city', $this->request_id ),
				'state'   => get_field( 'customer_request_seller_customer_request_property_address_state', $this->request_id ),
				'zipcode' => get_field( 'customer_request_seller_customer_request_property_address_zipcode', $this->request_id ),
			),
		);

		return $seller;

	}

	public function get_request_buyer_parameters() {
		$buyer = array(
			'property_type' => $this->get_request_type_property_type( 'buyer' ),
			'price_range'   => $this->get_request_price( 'buyer' ),
			'how_soon'      => get_field( 'customer_request_buyer_customer_request_how_soon', $this->request_id ),
			'preapproved'   => get_field( 'customer_request_buyer_customer_request_preapproved_by_lender', $this->request_id ),
			'address'       => array(
				'zipcode' => get_field( 'customer_request_buyer_customer_request_customer_zip', $this->request_id ),
			),
		);

		return $buyer;
	}

	public function get_request_type_property_type( $type ) {
		return get_field( 'customer_request_' . $type . '_customer_request_property_type', $this->request_id );
	}

	public function get_request_type_property_type_text() {

		if ( 'Seller & Buyer' === $this->request_type ) {
			$request_type_text  = '<div><strong>Seller</strong><br>';
			$request_type_text .= $this->get_request_type_property_type( 'seller' ) . '</div>';
			$request_type_text .= '<div><strong>Buyer</strong><br>';
			$request_type_text .= $this->get_request_type_property_type( 'buyer' ) . '</div>';
		} else {
			$property          = $this->get_request_type_property_type( strtolower( $this->request_type ) );
			$request_type_text = '<div><strong>' . $this->request_type . '</strong><br>' . $property . '</div>';
		}

		return $request_type_text;
	}

	public function get_request_address( $type ) {

		switch ( $type ) {
			case 'seller':
				$address_sell_arr = $this->seller['address'];

				if ( is_array( $address_sell_arr ) ) {
					$address_sell = '';
					foreach ( $address_sell_arr as $key => $value ) {
						if ( '' === $value ) {
							continue;
						}

						$address_sell .= $value;

						if ( 'state' === $key || 'street' === $key ) {
							$address_sell .= ', ';
						} elseif ( 'zipcode' === $key ) {

						} else {
							$address_sell .= ',<br>';
						}
					}
				} else {
					$address_sell = 'None';
				}

				$address_text = $address_sell;
				break;

			case 'buyer':
				$address_text = get_field( 'customer_request_buyer_customer_request_customer_zip', $this->request_id );
				break;

			default:
				$address_text = 'None';
		}

		return $address_text;
	}

	public function get_request_address_text() {

		switch ( $this->request_type ) {
			case 'Seller & Buyer':
				$address_text  = '<div><strong>Seller</strong><br>';
				$address_text .= $this->get_request_address( 'seller' ) . '</div>';
				$address_text .= '<div><strong>Buyer</strong><br>';
				$address_text .= $this->get_request_address( 'buyer' ) . '</div>';
				break;

			case 'Seller':
				$address_text  = '<strong>Seller</strong><br>';
				$address_text .= $this->get_request_address( 'seller' );
				break;

			case 'Buyer':
				$address_text  = '<strong>Buyer</strong><br>';
				$address_text .= $this->get_request_address( 'buyer' );
				break;

			default:
				$address_text = 'None';
		}

		return $address_text;

	}

	public function get_request_price( $type ) {

		return get_field( 'customer_request_' . $type . '_customer_request_price_range', $this->request_id );
	}

	public function get_request_price_text() {
		$request_id = $this->request_id;

		switch ( $this->request_type ) {
			case 'Seller & Buyer':
				$price_text  = '<div><strong>Seller</strong><br>';
				$price_text .= $this->get_request_price( 'seller' ) . '</div>';
				$price_text .= '<div><strong>Buyer</strong><br>';
				$price_text .= $this->get_request_price( 'buyer' ) . '</div>';
				break;

			case 'Seller':
				$price_text = $this->get_request_price( 'seller' );
				break;

			case 'Buyer':
				$price_text = $this->get_request_price( 'buyer' );
				break;

			default:
				$price_text = 'None';
		}

		return $price_text;

	}

	public function change_request_status_callback() {

		$request_id = intval( $_POST['request_id'] );
		$status     = $_POST['new_status'];

		if ( ! current_user_can( 'manage_options' ) ) {
			wp_send_json_error( array( 'message' => 'Permission Denied' ), 403 );
		}

		// Check IDs of agent and request
		if ( ! is_numeric( $request_id ) ) {
			wp_send_json_error( 'Request ID parameter is not valid', 400 );
		}

		// Check IDs of agent and request
		if ( ! isset( $this->available_statuses[ $status ] ) ) {
			wp_send_json_error( 'Status parameter is not valid', 400 );
		}

		update_field( 'customer_request_status', $this->available_statuses[ $status ], $request_id );

		wp_send_json_success( array( 'message' => 'Updated' ), 200 );
	}

	public function get_wp_id_from_custom( $custom_id ) {
		$request = get_posts(
			array(
				'post_type'      => 'request',
				'posts_per_page' => 1,
				'meta_query'     => array(
					array(
						'key'     => 'customer_request_id',
						'value'   => $custom_id,
						'compare' => '=',
					),
				),
			)
		);

		if ( $request && is_array( $request ) ) {
			return $request[0]->ID;
		}

		return false;

	}
}
