<?php

namespace Agents_Backend;

/**
 * The class containing informatin about the plugin.
 */
class Email {

	private $plugin_slug;
	private $version;

	public function __construct( $plugin_slug, $version ) {
		$this->plugin_slug = $plugin_slug;
		$this->version     = $version;
	}

	/**
	 * Register Request custom post type
	 *
	 * @return void
	 */
	public function register_email_template_cpt() {

		$labels = array(
			'name'               => _x( 'Email Template', 'post type general name', $this->plugin_slug ),
			'singular_name'      => _x( 'Email Template', 'post type singular name', $this->plugin_slug ),
			'menu_name'          => _x( 'Email Templates', 'admin menu', $this->plugin_slug ),
			'name_admin_bar'     => _x( 'Template', 'add new on admin bar', $this->plugin_slug ),
			'add_new'            => _x( 'Add New', 'template', $this->plugin_slug ),
			'add_new_item'       => __( 'Add New Template', $this->plugin_slug ),
			'new_item'           => __( 'New Template', $this->plugin_slug ),
			'edit_item'          => __( 'Edit Template', $this->plugin_slug ),
			'view_item'          => __( 'View Template', $this->plugin_slug ),
			'all_items'          => __( 'All Templates', $this->plugin_slug ),
			'search_items'       => __( 'Search Templates', $this->plugin_slug ),
			'parent_item_colon'  => __( 'Parent Templates:', $this->plugin_slug ),
			'not_found'          => __( 'No Templates found.', $this->plugin_slug ),
			'not_found_in_trash' => __( 'No Templates found in Trash.', $this->plugin_slug ),
		);

		$args = array(
			'labels'              => $labels,
			'description'         => __( 'Template custom poost type.', $this->plugin_slug ),
			'public'              => true,
			'exclude_from_search' => true,
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => false,
			'capability_type'     => 'post',
			'has_archive'         => false,
			'hierarchical'        => false,
			'menu_position'       => 22,
			'supports'            => array( 'title', 'editor' ),
			'menu_icon'           => 'dashicons-media-document',
		);

		register_post_type( 'template', $args );

	}

	/**
	 * Register Request custom post type
	 *
	 * @return void
	 */
	public function register_system_email_template_cpt() {

		$labels = array(
			'name'               => _x( 'System Emails', 'post type general name', $this->plugin_slug ),
			'singular_name'      => _x( 'System Email', 'post type singular name', $this->plugin_slug ),
			'menu_name'          => _x( 'System Emails', 'admin menu', $this->plugin_slug ),
			'name_admin_bar'     => _x( 'System Email', 'add new on admin bar', $this->plugin_slug ),
			'add_new'            => _x( 'Add New', 'template', $this->plugin_slug ),
			'add_new_item'       => __( 'Add New System Email', $this->plugin_slug ),
			'new_item'           => __( 'New System Email', $this->plugin_slug ),
			'edit_item'          => __( 'Edit System Email', $this->plugin_slug ),
			'view_item'          => __( 'View System Email', $this->plugin_slug ),
			'all_items'          => __( 'All System Emails', $this->plugin_slug ),
			'search_items'       => __( 'Search System Emails', $this->plugin_slug ),
			'parent_item_colon'  => __( 'Parent System Emails:', $this->plugin_slug ),
			'not_found'          => __( 'No System Emails found.', $this->plugin_slug ),
			'not_found_in_trash' => __( 'No System Emails found in Trash.', $this->plugin_slug ),
		);

		$args = array(
			'labels'              => $labels,
			'description'         => __( 'System Email custom poost type.', $this->plugin_slug ),
			'public'              => true,
			'exclude_from_search' => true,
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => false,
			'capability_type'     => 'post',
			'has_archive'         => false,
			'hierarchical'        => false,
			'menu_position'       => 22,
			'supports'            => array( 'title', 'editor' ),
			'menu_icon'           => 'dashicons-media-document',
		);

		register_post_type( 'system-email', $args );

	}

	/**
	 * Send email using wp_mail() function
	 *
	 * @param string $email_type type
	 * @param array  $params Email message parameters
	 * @param array  $data
	 */
	public static function send_email( $email_type, $params = array(), $data = array() ) {
		$to   = $params['to'];
		$from = 'AkamaiAgent <nick@akamaiagent.com>';

		$headers = 'From: ' . $from . "\r\n" .
				   'Cc: nick@akamaiagent.com' . "\r\n" .
				   'content-type: text/html';

		switch ( $email_type ) {
			// Agent emails.
			case 'agent-new-application':
				$to      = $params['first_name'] . ' ' . $params['last_name'] . '< ' . $params['to'] . ' >';
				$subject = 'Thank you for your interest!';
				break;

			case 'agent-profile-approved':
				$subject = 'You AkamaiAgent application has been approved';
				break;

			case 'agent-profile-rejected':
				$subject = 'You AkamaiAgent application has been rejected';
				break;

			case 'agent-profile-registered':
				$subject = 'AkamaiAgent Account Setup';
				$rp_link = self::generate_reset_url( $params['user_id'] );
				break;
			case 'agent-profile-completed':
				$subject = 'Profile Page Complete!';
				break;
			case 'agent-new-customer-referral':
				$to      = $params['agent_first_name'] . ' ' . $params['agent_last_name'] . '< ' . $params['to'] . ' >';
				$subject = 'Customer Referral!';
				break;
			case 'agent-contact':
				$from    = 'AA - Customer Contact ' . ' <' . $params['from'] . '>';
				$to      = $params['agent_first_name'] . ' ' . $params['agent_last_name'] . '< ' . $params['to'] . ' >';
				$headers = 'From: ' . $from . "\r\n" .
						   'Cc: nick@akamaiagent.com' . "\r\n" .
						   'Reply-To: ' . $params['from'] . "\r\n" .
						   'content-type: text/html';

				$subject = 'Customer Contact';
				break;

			case 'agent-vacation-status-changed':
				if ( $params['vacation_status'] === 'yes' ) {
					$subject = 'Vacation Status "ON"!';
				} else {
					$subject = 'Vacation Status "OFF"!';
				}

				break;

			// Customer emails.
			case 'customer-new-request':
				$email_type = '/customer-request-emails/' . $email_type;
				$subject    = 'Welcome to AkamaiAgent';
				break;
			case 'customer-assigned-agents':
				$subject = 'AkamaiAgent Profiles';
				break;
			case 'customer-confirmation-email':
				$subject = 'Confirm your email on AkamaiAgent';
				break;

			// Mass emails.
			case 'any-user':
				$subject = $params['subject'];
				break;
			case 'all-users-message':
				$subject = $params['subject'];
				break;

		}

		ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'templates/emails/email-header.php';
		include plugin_dir_path( dirname( __FILE__ ) ) . 'templates/emails/' . $email_type . '.php';
		include plugin_dir_path( dirname( __FILE__ ) ) . 'templates/emails/email-footer.php';

		$message = ob_get_contents();
		ob_end_clean();

		$result = wp_mail( $to, $subject, $message, $headers );

		return $result;
	}

	/**
	 * Send email using wp_mail() function
	 *
	 * @param $email_type
	 * @param array      $params Email message parameters
	 * @param array      $data
	 *
	 * @return void
	 */
	public static function send_admin_email( $email_type, $params = array(), $data = array() ) {

		$to      = get_option( 'admin_email' );
		$from    = 'From: akamaiagent.com <nick@akamaiagent.com>' . "\r\n";
		$headers = $from .
				   'content-type: text/html';

		switch ( $email_type ) {
			case 'admin-new-agent-application':
				$subject = 'New Agent application';
				break;
			case 'admin-new-customer-request':
				$subject = 'Customer Request - ' . $params['first_name'];
				break;
		}

		ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'templates/emails/email-header.php';
		include plugin_dir_path( dirname( __FILE__ ) ) . 'templates/emails/' . $email_type . '.php';
		include plugin_dir_path( dirname( __FILE__ ) ) . 'templates/emails/email-footer.php';

		$message = ob_get_contents();
		ob_end_clean();

		$result = wp_mail( $to, $subject, $message, $headers );
	}

	private static function generate_reset_url( $user_id ) {

		// Get user's reset password link
		$user      = get_user_by( 'id', $user_id );
		$user_name = $user->user_login;

		$reset_key = get_password_reset_key( $user );
		$rp_link   = wp_login_url() . '?action=rp&key=' . $reset_key . '&login=' . rawurlencode( $user_name );

		return $rp_link;

	}

	public function custom_retrieve_password_message( $message, $key, $user_login, $user_data ) {

		$message  = '<p>Aloha, ' . $user_data->first_name . '</p>';
		$message .= '<p>Please click <a href="' . site_url( "wp-login.php?action=rp&key=$key&login=" . rawurlencode( $user_login ), 'login' ) . '">here</a> to access your account and enter a new password</p>';
		$message .= '<p>Feel free to <a href="' . site_url( 'contact-us' ) . '">Contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our Akamai Agents!</p>';

		ob_start(); ?>
			<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
				<tbody>
				<tr>
					<td>
						<p>With warmest aloha,</p>
						<p>Nick Mann</p>
						<p>(808) 256-0245</p>
						<p><img style="border-radius:50%; width: 120px;" src="http://akamaiagent.com/wp-content/plugins/agents-backend/frontend/images/Nick%20Mann.jpg" alt="Nick Mann" /></p>
						<p>Hours of operation:</p>
						<p>Mon – Fri = 8:30 AM – 5:30 PM<br>
							Sat - Sun = 10:00 AM – 2:00 PM</p>
					</td>
				</tr>
				</tbody>
			</table>
		<?php

		$message .= ob_get_contents();
		ob_clean();

		return $message;
	}

	public static function send_to_many( $recipients = array(), $recipient_type = '', $subject = '', $message = '' ) {

		switch ( $recipient_type ) {
			case 'agents':
				foreach ( $recipients as $key => $recipient ) {
					$params = array(
						'to'         => $recipient->user_email,
						'first_name' => $recipient->first_name,
						'subject'    => $subject . ' - ' . $recipient->first_name . ' ' . $recipient->last_name,
						'message'    => $message,
					);
					self::send_email( 'all-agents-message', $params );
				}
				break;
			case 'everyone':
				foreach ( $recipients as $email => $recipient ) {
					$params = array(
						'to'         => $email,
						'first_name' => $recipient['first_name'],
						'subject'    => $subject . ' - ' . $recipient->first_name . ' ' . $recipient->last_name,
						'message'    => $message,
					);
					self::send_email( 'all-users-message', $params );
				}
				break;
			case 'customers':
				foreach ( $recipients as $email => $recipient ) {
					$params = array(
						'to'         => $email,
						'first_name' => $recipient['first_name'],
						'subject'    => $subject . ' - ' . $recipient['first_name'] . ' ' . $recipient['last_name'],
						'message'    => $message,
					);
					self::send_email( 'all-users-message', $params );
				}
				break;
			default:
				// code...
				break;
		}

	}

	public function set_html_content_type() {
		return 'text/html';
	}

	public function set_email_template_callback() {
		$template_id = intval( $_POST['template_id'] );

		$template = get_post( $template_id );

		if ( $template && ! is_wp_error( $template ) ) {
			wp_send_json_success( $template->post_content, 200 );
		}
	}

	/**
	 * Override default WP notification about password change.
	 *
	 * @param  mixed $wp_password_change_notification_email email obj.
	 * @param  mixed $user user obj.
	 * @param  mixed $blogname blogname.
	 */
	public function override_password_change_email( $wp_password_change_notification_email, $user, $blogname ) {
		$message = sprintf( __( 'Password changed for user: %1$s > %2$s' ), $user->display_name, $user->user_login ) . "\r\n";

		$wp_password_change_notification_email['message'] = $message;

		return $wp_password_change_notification_email;
	}
}
