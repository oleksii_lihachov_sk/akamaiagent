<div class="edit-account entry-content entry">

	<?php if ( isset( $_POST ) && ! empty( $_POST ) ): ?>
		<div class="message">
			<?php if ( $error ): ?>
				<?php echo '<p class="error">' . implode("<br />", $error) . '</p>'; ?>
			<?php else : ?>
				<p class="success"><?php _e('Account Updated', 'agents-backend'); ?></p>
			<?php endif; ?>
		</div>
	<?php endif ?>

    <form method="post" id="adduser" action="<?php the_permalink(); ?>">

        <p class="form-username">
            <label for="first-name"><?php _e('First Name', 'agents-backend'); ?></label>
            <input class="text-input" name="first-name" type="text" id="first-name" value="<?php the_author_meta( 'first_name', $current_user->ID ); ?>" />
        </p><!-- .form-username -->
        <p class="form-username">
            <label for="last-name"><?php _e('Last Name', 'agents-backend'); ?></label>
            <input class="text-input" name="last-name" type="text" id="last-name" value="<?php the_author_meta( 'last_name', $current_user->ID ); ?>" />
        </p><!-- .form-username -->
        <p class="form-email">
            <label for="email"><?php _e('E-mail *', 'agents-backend'); ?></label>
            <input class="text-input" name="email" type="text" id="email" value="<?php the_author_meta( 'user_email', $current_user->ID ); ?>" />
        </p><!-- .form-email -->
        <p class="form-password">
            <label for="pass1"><?php _e('Password *', 'agents-backend'); ?> </label>
            <input class="text-input" name="pass1" type="password" id="pass1" />
        </p><!-- .form-password -->
        <p class="form-password">
            <label for="pass2"><?php _e('Repeat Password *', 'agents-backend'); ?></label>
            <input class="text-input" name="pass2" type="password" id="pass2" />
        </p><!-- .form-password -->

        <p class="form-submit">
            <input name="updateuser" type="submit" id="updateuser" class="submit button" value="<?php _e('Update', 'agents-backend'); ?>" />
            <?php wp_nonce_field( 'update_account_nonce', 'update-account-nonce' ) ?>
            <input name="user_id" type="hidden" id="user_id" value="<?php echo $current_user->ID; ?>" />
            <input name="action" type="hidden" id="action" value="update-account" />
        </p><!-- .form-submit -->
    </form><!-- #adduser -->
</div><!-- .entry-content -->