<?php 

	$request_class = new \Agents_Backend\Request( $request_id );

	$type = get_field( 'customer_request_customer_type', $request_id );
	$type_slug = strtolower($type);

	$language = '';
	if ( get_field( 'customer_request_need_language', $request_id ) !== 'No' ) {
		$language = get_field('customer_request_other_language', $request_id );
	} else {
		$language = 'English';
	}
?>

<table id="request-data-table">
	<thead>
		<tr>
			<th>Type of Customer / Property</th>
			<th>Zip Code</th>
			<th>Estimated home value or buyer price range</th>
			<th>Language</th>
		</tr>
	</thead>
	<tbody>
		<?php if ( 'Seller & Buyer' === $type ): ?>
			<tr>
				<td>Seller - <?php the_field( 'customer_request_seller_customer_request_property_type', $request_id ); ?></td>
				<td><?php echo $request_class->get_request_address( 'seller' ); ?></td>
				<td><?php the_field( 'customer_request_seller_customer_request_price_range', $request_id ); ?></td>
				<td><?php echo $language; ?></td>
			</tr>
			<tr>
				<td>Buyer - <?php the_field( 'customer_request_buyer_customer_request_property_type', $request_id ); ?></td>
				<td><?php the_field( 'customer_request_buyer_customer_request_customer_zip', $request_id ); ?></td>
				<td><?php the_field( 'customer_request_buyer_customer_request_price_range', $request_id ); ?></td>
				<td><?php echo $language; ?></td>
			</tr>

		<?php else: ?>
			
			<tr>
				<td><?php echo $type; ?> - <?php the_field( 'customer_request_' . $type_slug . '_customer_request_property_type', $request_id ); ?></td>
				<?php if ( 'Seller' === $type ): ?>
					<td><?php the_field( 'customer_request_' . $type_slug . '_customer_request_property_address_zipcode', $request_id ); ?></td>
				<?php else: ?>
					<td><?php the_field( 'customer_request_' . $type_slug . '_customer_request_customer_zip', $request_id ); ?></td>
				<?php endif; ?>
				<td><?php the_field( 'customer_request_' . $type_slug . '_customer_request_price_range', $request_id ); ?></td>
				<td><?php echo $language; ?></td>
			</tr>

		<?php endif ?>
	</tbody>
</table>


