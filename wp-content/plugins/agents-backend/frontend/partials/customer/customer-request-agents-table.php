<table id="available-agents-table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Company</th>
			<th>Languages</th>
			<th>Zip Codes</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php if ( ! empty( $agents ) ): ?>
			<?php foreach ( $agents as $agent ): ?>
				<?php 
					$agent_class = new \Agents_Backend\Agent( $agent->ID );
				?>
				<tr>
					<td><span class="agent-name detailed-profile" data-agent-id="<?php echo $agent->ID; ?>"><?php echo $agent_class->first_name; ?> <?php echo $agent_class->last_name; ?></span></td>
					<td><?php echo $agent_class->company_name; ?></td>
					<td><?php echo $agent_class->language; ?></td>
					<td><?php echo $agent_class->zipcodes; ?></td>
					<td><button class="primary select-agent" data-agent-id="<?php echo $agent->ID; ?>">Select</button></td>
				</tr>
			<?php endforeach ?>
		<?php endif; ?>
	</tbody>
</table>