<form action="" method="get" class="requests-form">
	<div class="form-group">
		<div class="field-wrap">
		    <label for="request-id">Request ID</label>
		    <input type="number" class="form-control" id="request-id" name="request-id" value="<?php echo ( isset( $_GET['request-id'] ) ) ? $_GET['request-id'] : ''; ?>" placeholder="Request ID">
		</div>
		<div class="field-wrap">
		 	<label for="request-date">Request Date</label>
		    <input type="date" class="form-control" id="request-date" name="request-date" value="<?php echo ( isset( $_GET['request-date'] ) ) ? $_GET['request-date'] : ''; ?>">
		</div>
		<div class="field-wrap">
		 	<label for="request-status">Request Status</label>
		    <select name="request-status" id="customer-type">
		    	<option value="">---</option>
		    	<option value="Pending" <?php echo ( isset( $_GET['request-status'] ) && $_GET['request-status'] === 'Pending' ) ? 'selected' : ''; ?>>Pending</option>
		    	<option value="Active" <?php echo ( isset( $_GET['request-status'] ) && $_GET['request-status'] === 'Active' ) ? 'selected' : ''; ?>>Active</option>
		    	<option value="Sent" <?php echo ( isset( $_GET['request-status'] ) && $_GET['request-status'] === 'Sent' ) ? 'selected' : ''; ?>>Sent</option>
		    	<option value="Deactivated" <?php echo ( isset( $_GET['request-status'] ) && $_GET['request-status'] === 'Deactivated' ) ? 'selected' : ''; ?>>Deactivated</option>
		    	<option value="Deleted" <?php echo ( isset( $_GET['request-status'] ) && $_GET['request-status'] === 'Deleted' ) ? 'selected' : ''; ?>>Deleted</option>
		    	<option value="Closed – Pending Payment" <?php echo ( isset( $_GET['request-status'] ) && $_GET['request-status'] === 'Closed – Pending Payment' ) ? 'selected' : ''; ?>>Closed – Pending Payment</option>
		    	<option value="Closed – Paid" <?php echo ( isset( $_GET['request-status'] ) && $_GET['request-status'] === 'Closed – Paid' ) ? 'selected' : ''; ?>>Closed – Paid</option>
		    </select>
		</div>
	</div>
	
	<div class="form-group">
		<div class="field-wrap">
		    <label for="customer-name">Customer Name</label>
		    <input type="text" class="form-control" id="customer-name" name="customer-name" value="<?php echo ( isset( $_GET['customer-name'] ) ) ? $_GET['customer-name'] : ''; ?>" placeholder="Customer Name">
		</div>
		<div class="field-wrap">
		    <label for="customer-email">Customer Email</label>
		    <input type="text" class="form-control" id="customer-email" name="customer-email" value="<?php echo ( isset( $_GET['customer-email'] ) ) ? $_GET['customer-email'] : ''; ?>" placeholder="Customer Email">
		</div>
		<div class="field-wrap">
		 	<label for="customer-type">Customer Type</label>
		    <select name="customer-type" id="customer-type">
		    	<option value="">---</option>
		    	<option value="Seller" <?php echo ( isset( $_GET['customer-type'] ) && $_GET['customer-type'] === 'Seller' ) ? 'selected' : ''; ?>>Seller</option>
		    	<option value="Buyer" <?php echo ( isset( $_GET['customer-type'] ) && $_GET['customer-type'] === 'Buyer' ) ? 'selected' : ''; ?>>Buyer</option>
		    	<option value="Seller & Buyer" <?php echo ( isset( $_GET['customer-type'] ) && $_GET['customer-type'] === 'Seller & Buyer' ) ? 'selected' : ''; ?>>Seller & Buyer</option>
		    </select>
		</div>
	</div>
	
	<div class="form-group">
		<div class="field-wrap">
		 	<label for="property-type">Type of property</label>
		    <select name="property-type" id="property-type">
		    	<option value="">---</option>
		    	<option value="Single Family Home" <?php echo ( isset( $_GET['property-type'] ) && $_GET['property-type'] === 'Single Family Home' ) ? 'selected' : ''; ?>>Single Family Home</option>
		    	<option value="Condo/Townhouse" <?php echo ( isset( $_GET['property-type'] ) && $_GET['property-type'] === 'Condo/Townhouse' ) ? 'selected' : ''; ?>>Condo/Townhouse</option>
		    	<option value="Vacant Land" <?php echo ( isset( $_GET['property-type'] ) && $_GET['property-type'] === 'Vacant Land' ) ? 'selected' : ''; ?>>Vacant Land</option>
		    </select>
		</div>
		<div class="field-wrap">
		 	<label for="property-price">Price range of property</label>
		    <select name="property-price" id="property-price">
		    	<option value="">---</option>
		    	<option value="$300K or less" <?php echo ( isset( $_GET['property-price'] ) && $_GET['property-price'] === '$300K or less' ) ? 'selected' : ''; ?>>$300K or less</option>
		    	<option value="$301K - $500K" <?php echo ( isset( $_GET['property-price'] ) && $_GET['property-price'] === '$301K - $500K' ) ? 'selected' : ''; ?>>$301K - $500K</option>
		    	<option value="$501K - $750K" <?php echo ( isset( $_GET['property-price'] ) && $_GET['property-price'] === '$501K - $750K' ) ? 'selected' : ''; ?>>$501K - $750K</option>
		    	<option value="$751K - $950K" <?php echo ( isset( $_GET['property-price'] ) && $_GET['property-price'] === '$751K - $950K' ) ? 'selected' : ''; ?>>$751K - $950K</option>
		    	<option value="$951K - $1.5M" <?php echo ( isset( $_GET['property-price'] ) && $_GET['property-price'] === '$951K - $1.5M' ) ? 'selected' : ''; ?>>$951K - $1.5M</option>
		    	<option value="$1.5M – $3M" <?php echo ( isset( $_GET['property-price'] ) && $_GET['property-price'] === '$1.5M – $3M' ) ? 'selected' : ''; ?>>$1.5M – $3M</option>
		    	<option value="$3M and more" <?php echo ( isset( $_GET['property-price'] ) && $_GET['property-price'] === '$3M and more' ) ? 'selected' : ''; ?>>$3M and more</option>
		    </select>
		</div>
		<div class="form-group">
		    <label for="zipcode">Zip Code</label>
		    <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zip Code" value="<?php echo ( isset( $_GET['zipcode'] ) ) ? $_GET['zipcode'] : ''; ?>">
		</div>
	</div>
	<input type="submit" class="btn btn-primary" name="filter" value="Filter">
	<input type="submit" id="clear" class="btn btn-primary" name="clear" value="Clear Search Criteria">
</form>