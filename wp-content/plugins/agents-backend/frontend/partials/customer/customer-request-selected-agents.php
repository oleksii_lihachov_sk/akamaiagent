<?php 

	$selected_agents = get_field( 'customer_request_assigned_agents', $request_id );

	if ( ! $selected_agents || empty( $selected_agents ) ) {
		return false;
	}
?>

<div class="selected-agents-wrap row">
	<?php foreach ( $selected_agents as $key => $user_id ): ?>
		<?php 
			$agent_class = new \Agents_Backend\Agent( $user_id );
			$agent       = new WP_User($user_id);
			$photo 	     = get_field( 'agent_photo', 'user_' . $user_id );
		?>
		<div class="col-12 col-lg-4 mb-md-0 mb-3">
			<div class="row p-0 m-0 align-items-stretch">
				<div class="col-12 col-md-5 p-3">
					<div class="img-wrap">
						<img src="<?php echo $photo['sizes']['agent-photo']; ?>" alt="">
					</div>
				</div>
				<div class="col-12 col-md-7 d-flex flex-column p-3">
					<h4><?php echo $agent_class->first_name; ?> <?php echo $agent_class->last_name; ?></h4>

					<p><strong>Company:</strong></p>
					<p class="mb-md-0 mb-2"><?php echo $agent_class->company_name; ?></p>
					<div class="actions d-flex justify-content-between">
						<button class="primary detailed-profile" data-agent-id="<?php echo $user_id; ?>">Detail</button>
						<button class="remove danger unselect-agent" data-agent-id="<?php echo $user_id; ?>">Remove</button>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>