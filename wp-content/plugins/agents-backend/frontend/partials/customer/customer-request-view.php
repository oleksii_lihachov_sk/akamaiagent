<?php

	if ( ! isset( $_GET['request_id'] ) ) {
		echo '<h1>There is no this Request</h1>';
	}

	// Change custom ID to real WP ID
	$request_id      = $request_class->request_id;

	$selected_agents = $request_class->assigned_agents;

	$agents          = \Agents_Backend\Request::get_best_match_agents( $request_id );

?>

<div class="request container" data-request-id="<?php echo $request_id; ?>">
	<div class="request-heading">
		<h1><?php _e( 'Request', 'agents-backend' ); ?> #<?php echo $request_class->request_custom_id; ?></h1>
		<p>Published: <?php echo get_the_date( 'm/d/Y', $request_id ); ?></p>
	</div>

	<section class="agents-search">
		<h2><?php _e( 'Admin Search', 'agents-backend' ); ?><i class="fa fa-angle-down" aria-hidden="true"></i><i class="fa fa-angle-up" aria-hidden="true"></i></h2>
		<?php include 'customer-request-agents-search.php' ?>
	</section>
	
	<section class="agents-table">
		<h2><?php _e( 'Agent Search Results', 'agents-backend' ); ?></h2>

		<?php include 'customer-request-data-table.php' ?>

		<div class="available-agents">
			<?php if ( $agents ): ?>
				<?php include 'customer-request-agents-table.php' ?>
			<?php else: ?>
				<h2><?php _e( 'No available agents', 'agents-backend' ); ?></h2>
			<?php endif; ?>
		</div>
	</section>

	<section class="detailed-agent">
		<h2><?php _e( 'Agent detail profile', 'agents-backend' ); ?></h2>
		<div id="detailed-content"></div>
	</section>

	<section class="selected-agents">
		<h2><?php _e( 'Selected Agents', 'agents-backend' ); ?></h2>
		<div id="selected-agents">
			<?php if ( $selected_agents ): ?>
				<?php include 'customer-request-selected-agents.php' ?>
			<?php endif ?>
		</div>
	</section>

	<div class="send-button">
		<button id="send_to_customer" class="primary" data-request-id="<?php echo $request_id; ?>"><?php _e( 'Send to customer', 'agents-backend' ); ?></button>
	</div>
</div>