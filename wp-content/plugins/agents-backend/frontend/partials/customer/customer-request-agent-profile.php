<?php 
	
	if ( ! isset($agent) || ! $agent ) {
		return false;
	}

	$agent_class = new \Agents_Backend\Agent( $agent->ID );

	$photo 	 = get_field( 'agent_photo', 'user_' . $agent_class->agent_id );
	$socials = get_field( 'agent_socials', 'user_' . $agent_class->agent_id );
?>

<div class="row">
	<div class="col-12 col-lg-3">
		<div class="image-wrap">
			<img src="<?php echo $photo['sizes']['agent-photo']; ?>" alt="">
		</div>
		<button class="btn primary select-agent" data-agent-id="<?php echo $agent_class->agent_id; ?>">Select</button>
	</div>
	<div class="col-12 col-lg-9">
		<h3><?php echo $agent_class->first_name; ?> <?php echo $agent_class->last_name; ?></h3>

		<ul class="socials">
			<?php foreach ( $socials as $key => $value ): ?>
				
				<?php if ( ! empty( $value ) ): ?>

					<?php if ( 'zillow' === $key ): ?>
						<li><a href="<?php echo $value; ?>" target="_blank"><?php _e( 'Zillow', 'agents-backend' ); ?></a></li>
					<?php else: ?>
						<li><a href="<?php echo $value; ?>" target="_blank"><i class="fa fa-<?php echo $key; ?>" aria-hidden="true"></i></a></li>
					<?php endif; ?>

				<?php endif ?>
				
			<?php endforeach ?>
		</ul>

		<?php echo \Agents_Backend\Agent::render_agent_yelp_rating(  get_field( 'agent_ratings_link_yelp', 'user_' . $agent_class->agent_id ) ); ?>
		<?php echo \Agents_Backend\Agent::render_agent_zillow_rating( get_field( 'agent_ratings_link_zillow', 'user_' . $agent_class->agent_id ) ); ?>
		
		<div class="info row">
			<div class="col-12 col-lg-6">
				<p><strong><?php _e( 'Company', 'agents-backend' ); ?>:</strong> <?php echo $agent_class->company_name; ?></p>
				<p><strong><?php _e( 'Speaks', 'agents-backend' ); ?>:</strong> <?php echo $agent_class->language; ?></p>
				<p><strong><?php the_field( 'agent_transactions_in_last_year', 'user_' . $agent->ID ); ?></strong> <?php _e( 'Transactions in Last 12 Months', 'agents-backend' ); ?></p>
				<p><strong><?php _e( 'Certifications', 'agents-backend' ); ?>: </strong><?php echo ( is_array( get_field( 'agent_certifications', 'user_' . $agent->ID ) ) ) ? implode( ', ', get_field( 'agent_certifications', 'user_' . $agent->ID )) : ''; ?></p>
			</div>
			<div class="col-12 col-lg-6">
				<p><strong><?php _e( 'Agent License #', 'agents-backend' ); ?>:</strong> <?php the_field( 'agent_license_number', 'user_' . $agent->ID ); ?></p>
				<p><strong><?php the_field( 'agent_years_of_experience', 'user_' . $agent->ID ); ?></strong> <?php _e( 'Years of Experience', 'agents-backend' ); ?></p>
				<p><strong><?php _e( 'Specialtles', 'agents-backend' ); ?>: </strong><?php echo ( is_array( get_field( 'agent_specialtles', 'user_' . $agent->ID ) ) ) ? implode( ', ', get_field( 'agent_specialtles', 'user_' . $agent->ID )) : ''; ?></p>
				<p><strong><?php _e( 'Awards', 'agents-backend' ); ?>: </strong><?php the_field( 'agent_awards', 'user_' . $agent->ID ); ?></p>
			
			</div>
		</div>

		<div class="preferences row">
			<div class="col-12 col-lg-6">
				<h5>"Seller" preferences</h5>
				<ul>
					<?php foreach ( get_field( 'agent_seller_preferences', 'user_' . $agent->ID ) as $key => $value ): ?>
						<li><?php echo $value['label']; ?></li>
					<?php endforeach ?>
				</ul>

				<h5>Seller property value ranges</h5>
				<ul>
					<?php foreach ( get_field( 'agent_seller_property_value', 'user_' . $agent->ID ) as $key => $value ): ?>
						<li><?php echo $value['label']; ?></li>
					<?php endforeach ?>
				</ul>
			</div>
			<div class="col-12 col-lg-6">
				<h5>"Buyer" preferences</h5>
				<ul>
					<?php foreach ( get_field( 'agent_buyer_preferences', 'user_' . $agent->ID ) as $key => $value ): ?>
						<li><?php echo $value['label']; ?></li>
					<?php endforeach ?>
				</ul>

				<h5>Buyer property value ranges</h5>
				<ul>
					<?php foreach ( get_field( 'agent_buyer_property_value', 'user_' . $agent->ID ) as $key => $value ): ?>
						<li><?php echo $value['label']; ?></li>
					<?php endforeach ?>
				</ul>
			</div>
		</div>

		<div class="zip-codes mb-5">
			<h5>Agent areas</h5>
			<?php echo $agent_class->zipcodes; ?>
		</div>

		<div class="about-content">
			<?php the_field( 'agent_about', 'user_' . $agent->ID ); ?>
		</div>
	</div>
</div>