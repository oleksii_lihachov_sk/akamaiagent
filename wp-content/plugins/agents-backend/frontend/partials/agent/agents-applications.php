<div class="applications">
	<?php if ( isset( $result ) && $result ): ?>
		<div class="message alert alert-primary"><?php echo $result ?></div>
	<?php endif ?>

	<h1>New Applications</h1>

	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Agent Name</th>
				<th>Company name</th>
				<th>Status</th>
				<th colspan="2">Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $applications as $key => $application ): ?>
				<tr>
					<td><?php echo get_field( 'application_id', $application->ID ); ?></td>
					<td><?php echo get_the_date( 'm/d/Y', $application->ID ); ?></td>
					<td><?php echo get_field( 'agent_first_name', $application->ID ) . ' ' . get_field( 'agent_last_name', $application->ID ); ?></td>
					<td><?php echo get_field( 'agent_company_name', $application->ID ); ?></td>
					<td><?php echo get_field( 'application_status', $application->ID ); ?></td>
					<td><a href="?action=view&application_id=<?php echo $application->ID; ?>">View</a></td>
					<?php if ( get_field( 'application_status', $application->ID ) === 'Registered' ): ?>
						<td><a href="/agent-profile/?agent_id=<?php echo get_field( 'agent_user', $application->ID ); ?>">View Profile</a></td>
					<?php endif ?>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>