<?php if ( isset($_GET['customer_request_id']) ): ?>
	<div class="button-wrap">
		<a 
			id="contact-agent" 
			class="btn primary" 
			href="#" 
			data-agent-id="<?php echo $agent_class->agent_id; ?>" 
			data-agent-name="<?php echo $agent_class->first_name; ?>" 
			data-customer-request-id="<?php echo $_GET['customer_request_id']; ?>">
			<?php _e( 'Contact', 'agents-backend' ); ?> <?php echo $agent_class->first_name; ?>
		</a>
	</div>
	<div id="show-emails-popup">
		<div class="popup-wrap">
			<span class="close">X</span>
			<h4>Contacting this agent will allow them to see your email address, so that they may respond to your message.</h4>
			<div class="buttons">
				<a href="#" id="no">Cancel</a>
				<a href="#" id="yes">Continue</a>
			</div>
			<form action="">
				<textarea name="" id="" cols="30" rows="4"></textarea>
				<button>Send</button>
			</form>
		</div>
	</div>
<?php endif ?>