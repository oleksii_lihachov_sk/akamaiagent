<div class="agent-profile container">

	<div class="row">
		<div class="main col-12 col-md-12">
			<div class="row d-flex">

				<?php // Agent Photo ?>
				<div class="col-12 col-md-3 profile-info">

					<?php include plugin_dir_path(dirname(__FILE__)) . 'agent/agent-profile-photo.php'; ?>

					<?php include plugin_dir_path(dirname(__FILE__)) . 'agent/agent-profile-info.php'; ?>

					<?php if ( $agent_class->agent_id === get_current_user_id() ): ?>
                        <a href="?edit" class="btn alt success">Edit Profile</a>
					<?php endif ?>

				</div>

				<?php // Aboout Setion ?>
				<div class="col-12 col-md-9 bio">
					<h1><?php echo $agent_class->first_name; ?> <?php echo $agent_class->last_name; ?></h1>

					<?php echo $agent_class->render_agent_yelp_rating(); ?>

					<?php echo \Agents_Backend\Agent::render_agent_zillow_rating( get_field( 'agent_ratings_link_zillow', 'user_' . $agent_class->agent_id ) ); ?>

					<?php include plugin_dir_path(dirname(__FILE__)) . 'agent/agent-profile-socials.php'; ?>

					<?php include plugin_dir_path(dirname(__FILE__)) . 'agent/agent-profile-contact.php'; ?>

					<?php include plugin_dir_path(dirname(__FILE__)) . 'agent/agent-profile-about.php'; ?>

				</div>

			</div>
		</div>
	</div>
</div>