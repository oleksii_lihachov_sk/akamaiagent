<h3><?php _e( 'About', 'agents-backend' ); ?> <?php echo $agent->first_name; ?></h3>

<div class="about-content">
	<?php the_field( 'agent_about', 'user_' . $agent->ID ); ?>
</div>

<?php if ( $testimonials && ! empty($testimonials) ): ?>
	<div class="testimonials-section">
		<h3><?php _e( 'Testimonials', 'agents-backend' ); ?></h3>
		<div class="testimonials">
			<?php foreach ( $testimonials as $key => $testimonial ): ?>
				<div class="testimonial-wrap">
					<div class="testimonial">
						<p><?php echo $testimonial['text']; ?></p>
						<p class="author_name"><?php echo $testimonial['author_name']; ?></p>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
<?php endif ?>