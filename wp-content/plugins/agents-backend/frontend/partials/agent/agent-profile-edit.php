<div class="agent-profile edit container">
	<a href="/agent-profile/" class="btn alt success">Back to Profile</a>

	<?php if ( isset( $_POST ) && ! empty( $_POST ) ): ?>
		<div class="message">
			<p class="success"><?php _e('Profile Updated', 'agents-backend'); ?></p>
		</div>
	<?php endif ?>

	<?php acf_form( array(
		'id' => 'edit-profile',
		'post_id' => 'user_' . $agent->ID,
		'fields' => array(
			'field_5cd862489ebd9',
			'agent_photo',
			'agent_first_name',
			'agent_last_name',
			'agent_specialtles',
			'agent_certifications',
			'agent_awards',
			'agent_license_number',
			'field_5cc5ca4034027',
			'agent_company_name',
			'agent_company_phone',
			'agent_company_address',
			'field_5cc5cadfdad11',
			'agent_years_of_experience',
			'agent_transactions_in_last_year',
			'agent_ratings_link',
			'field_5cc99768e66b9',
			'agent_socials',
			'agent_speaks_languages',
			'agent_about',
			'agent_testimonials',
			'field_5cd862019ebd7',
			'agent_zip',
			'agent_seller_preferences',
			'agent_seller_property_value',
			'agent_buyer_preferences',
			'agent_buyer_property_value'
		),
		'uploader' => 'wp',
		'return' => false
	) ); ?>
</div>