<?php 
	$application_id = $_GET['application_id'];
?>

<h1>Application #<?php echo get_field( 'application_id', $application_id ); ?></h1>

<table>
	<tbody>
			<tr>
				<td>Agent Name</td>
				<td><?php echo get_field( 'agent_first_name', $application_id ) . ' ' . get_field( 'agent_last_name', $application_id ); ?></td>
			</tr>
			<tr>
				<td>Agent License Number</td>
				<td><?php echo get_field( 'agent_license_number', $application_id ); ?></td>
			</tr>
			<tr>
				<td>Agent Email</td>
				<td><?php echo get_field( 'agent_email', $application_id ); ?></td>
			</tr>
			<tr>
				<td>Agent Phone (Office)</td>
				<td><?php echo get_field( 'agent_phone_office', $application_id ); ?></td>
			</tr>
			<tr>
				<td>Agent Phone (Mobile)</td>
				<td><?php echo get_field( 'agent_phone_mobile', $application_id ); ?></td>
			</tr>

			<tr>
				<td colspan="2" class="heading"><strong>Company</strong></td>
			</tr>
			<tr>
				<td>Company Name</td>
				<td><?php echo get_field( 'agent_company_name', $application_id ); ?></td>
			</tr>
			<tr>
				<td>Company Phone</td>
				<td><?php echo get_field( 'agent_company_phone', $application_id ); ?></td>
			</tr>
			<tr>
				<td>Company Address</td>
				<?php if ( is_array( get_field( 'agent_company_address', $application_id ) ) ): ?>
					<td><?php echo implode( ', ', get_field( 'agent_company_address', $application_id ));  ?></td>
				<?php else : ?>
					<td><?php echo get_field( 'agent_company_address', $application_id );  ?></td>
				<?php endif ?>
			</tr>
			<tr>
				<td colspan="2" class="heading"><strong>Experience</strong></td>
			</tr>
			<tr>
				<td>I maintain a valid Hawaii real estate license in good standing.</td>
				<td><?php echo ( is_array( get_field( 'agent_valid_license', $application_id ) ) ) ? 'yes' : 'no'; ?></td>
			</tr>
			<tr>
				<td>I have been a licensed Hawaii real estate agent for a minimum of three (3) years.</td>
				<td><?php echo ( is_array( get_field( 'agent_licensed_3_years', $application_id ) ) ) ? 'yes' : 'no'; ?></td>
			</tr>
			<tr>
				<td>I have closed a minimum of 10 transactions (sides) during the last 12 months.</td>
				<td><?php echo ( is_array( get_field( 'agent_10_transactiions', $application_id ) ) ) ? 'yes' : 'no'; ?></td>
			</tr>
			<tr>
				<td>I maintain a minimum 4 star rating on Yelp or Zillow with a minimum of 4 reviews.</td>
				<td><?php echo ( is_array( get_field( 'agent_4_stars_rating', $application_id ) ) ) ? 'yes' : 'no'; ?></td>
			</tr>
			<tr>
				<td>Enter preferred Ratings page</td>
				<td></td>
			</tr>
			<tr>
				<td>Yelp</td>
				<td><?php echo get_field( 'agent_ratings_link_yelp', $application_id ); ?></td>
			</tr>
			<tr>
				<td>Zillow</td>
				<td><?php echo get_field( 'agent_ratings_link_zillow', $application_id ); ?></td>
			</tr>
			<tr>
				<td>I am a member of the National Association of Realtors (NAR)</td>
				<td><?php echo ( is_array( get_field( 'agent_nar_member', $application_id ) ) ) ? 'yes' : 'no'; ?></td>
			</tr>
			<tr>
				<td>Have any questions or comments? We’d love to hear from you!</td>
				<td><?php echo get_field( 'agent_comments', $application_id ); ?></td>
			</tr>
			<tr>
				<td colspan="2">
					<?php if ( get_field( 'application_status', $application_id ) === 'New' ): ?>
						<a class="btn success" href="?action=approve&application_id=<?php echo $application_id; ?>">Approve</a>
						<a class="btn danger" href="?action=reject&application_id=<?php echo $application_id; ?>">Reject</a>
					<?php endif ?>
					
					<?php if ( in_array( get_field( 'application_status', $application_id ), array( 'New', 'Approved' ) ) ): ?>
						<a class="btn primary" href="?action=register&application_id=<?php echo $application_id; ?>">Register</a>
					<?php endif ?>	
				</td>
			</tr>
	</tbody>
</table>