<div class="info">
	<p><?php the_field( 'agent_company_name', 'user_' . $agent->ID ); ?></p>
	<p><strong><?php _e( 'Agent License #', 'agents-backend' ); ?>:</strong> <?php the_field( 'agent_license_number', 'user_' . $agent->ID ); ?></p>
	<p><strong><?php _e( 'Speaks', 'agents-backend' ); ?>:</strong> <?php echo ( get_field( 'agent_speaks_languages', 'user_' . $agent->ID ) ) ? implode( ', ', get_field( 'agent_speaks_languages', 'user_' . $agent->ID )) : 'English'; ?></p>

	<p><strong><?php the_field( 'agent_years_of_experience', 'user_' . $agent->ID ); ?></strong> <?php _e( 'Years of Experience', 'agents-backend' ); ?></p>
	<p><strong><?php the_field( 'agent_transactions_in_last_year', 'user_' . $agent->ID ); ?></strong> <?php _e( 'Transactions in Last 12 Months', 'agents-backend' ); ?></p>

	<?php if ( get_field( 'agent_specialtles', 'user_' . $agent->ID ) ): ?>
		<p><strong><?php _e( 'Specialtles', 'agents-backend' ); ?>: </strong><?php echo ( is_array( get_field( 'agent_specialtles', 'user_' . $agent->ID ) ) ) ? implode( ', ', get_field( 'agent_specialtles', 'user_' . $agent->ID )) : ''; ?></p>
	<?php endif ?>

	<?php if ( get_field( 'agent_certifications', 'user_' . $agent->ID ) ): ?>
		<p><strong><?php _e( 'Certifications', 'agents-backend' ); ?>: </strong><?php echo ( is_array( get_field( 'agent_certifications', 'user_' . $agent->ID ) ) ) ? implode( ', ', get_field( 'agent_certifications', 'user_' . $agent->ID )) : ''; ?></p>
	<?php endif ?>

	<?php if ( get_field( 'agent_awards', 'user_' . $agent->ID ) ): ?>
		<p><strong><?php _e( 'Awards', 'agents-backend' ); ?>: </strong><?php the_field( 'agent_awards', 'user_' . $agent->ID ); ?></p>
	<?php endif ?>

</div>