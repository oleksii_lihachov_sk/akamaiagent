<?php if ( ! isset( $error ) || count( $error ) === 0 ): ?>
	<div class="biz-rating biz-rating-very-large">
	    <div class="i-stars <?php echo $rating_class; ?> rating-very-large" title="<?php echo $yelp_data['rating']; ?> star rating">
	        <img class="offscreen" height="303" src="https://s3-media2.fl.yelpcdn.com/assets/srv0/yelp_design_web/9b34e39ccbeb/assets/img/stars/stars.png" width="84" alt="<?php echo $yelp_data['rating']; ?> star rating">
	    </div>
	    <span class="review-count rating-qualifier">
	            <a href="<?php echo $yelp_data['url']; ?>"><?php echo ( $yelp_data['review_count'] > 1 ) ? $yelp_data['review_count'] . ' Yelp reviews' : $yelp_data['review_count'] . ' Yelp review' ?></a>
	    </span>
	</div>
<?php elseif( current_user_can('manage_options') || get_current_user_id() === $agent_id ): ?>
	<div class="message">
		<p>Yelp message, only you and admins see this message.</p>
		<?php foreach ( $error as $key => $value ): ?>
			<p class="error"><strong><?php echo $key ?></strong>: <?php echo $value ?></p>
		<?php endforeach ?>
	</div>
<?php endif ?>

