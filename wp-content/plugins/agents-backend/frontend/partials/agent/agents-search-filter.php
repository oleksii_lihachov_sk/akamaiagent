<div class="search-form">
	<form action="" method="get">
		<div class="fields-wrap two-cols">
			<h4>Type of Customer / Property:</h4>
			<label for="s_single_family">
				<input 
					type="checkbox" 
					id="s_single_family" 
					name="agent_seller_preferences" 
					value="single_family" 
					<?php echo ( isset( $_GET['agent_seller_preferences'] ) && $_GET['agent_seller_preferences'] === 'single_family' ) ? 'checked' : '' ?>>Seller - Single Family Residence</label>
			<label for="s_condo_townhouse">
				<input 
					type="checkbox" 
					id="s_condo_townhouse" 
					name="agent_seller_preferences" 
					value="condo_townhouse" 
					<?php echo ( isset( $_GET['agent_seller_preferences'] ) &&  $_GET['agent_seller_preferences'] === 'condo_townhouse' ) ? 'checked' : '' ?>>Seller - Condo / Townhouse</label>
			<label for="s_vacant_land">
				<input 
					type="checkbox" 
					id="s_vacant_land" 
					name="agent_seller_preferences" 
					value="vacant_land" 
					<?php echo ( isset( $_GET['agent_seller_preferences'] ) &&  $_GET['agent_seller_preferences'] === 'vacant_land' ) ? 'checked' : '' ?>>Seller - Vacant Land</label>
			<label for="b_single_family">
				<input 
					type="checkbox" 
					id="b_single_family" 
					name="agent_buyer_preferences" 
					value="single_family" 
					<?php echo ( isset( $_GET['agent_buyer_preferences'] ) &&  $_GET['agent_buyer_preferences'] === 'single_family' ) ? 'checked' : '' ?>>Buyer - Single Family Residence</label>
			<label for="b_condo_townhouse">
				<input 
					type="checkbox" 
					id="b_condo_townhouse" 
					name="agent_buyer_preferences" 
					value="condo_townhouse" 
					<?php echo ( isset( $_GET['agent_buyer_preferences'] ) &&  $_GET['agent_buyer_preferences'] === 'condo_townhouse' ) ? 'checked' : '' ?>>Buyer - Condo / Townhouse</label>
			<label for="b_vacant_land">
				<input 
					type="checkbox" 
					id="b_vacant_land" 
					name="agent_buyer_preferences" 
					value="vacant_land" 
					<?php echo ( isset( $_GET['agent_buyer_preferences'] ) &&  $_GET['agent_buyer_preferences'] === 'vacant_land' ) ? 'checked' : '' ?>>Buyer - Vacant Land</label>
		</div>

		<div class="fields-wrap two-cols">
			<h4>Estimated Home Value or Buyer Price Range:</h4>
			<label for="300K">
				<input 
					type="checkbox" 
					id="300K" 
					name="agent_property_value" 
					value="$300K or less" 
					<?php echo ( isset( $_GET['agent_property_value'] ) &&  $_GET['agent_property_value'] === 'vacant_land' ) ? 'checked' : '' ?>>$300K or less</label>
			<label for="301K">
				<input 
					type="checkbox" 
					id="301K" 
					name="agent_property_value" 
					value="$301K - $500K" 
					<?php echo ( isset( $_GET['agent_property_value'] ) &&  $_GET['agent_property_value'] === 'vacant_land' ) ? 'checked' : '' ?>>$301K - $500K</label>
			<label for="501K">
				<input 
					type="checkbox" 
					id="501K" 
					name="agent_property_value" 
					value="$501K - $750K" 
					<?php echo ( isset( $_GET['agent_property_value'] ) &&  $_GET['agent_property_value'] === 'vacant_land' ) ? 'checked' : '' ?>>$501K - $750K</label>
			<label for="751K">
				<input 
					type="checkbox" 
					id="751K" 
					name="agent_property_value" 
					value="$751K - $950K" 
					<?php echo ( isset( $_GET['agent_property_value'] ) &&  $_GET['agent_property_value'] === 'vacant_land' ) ? 'checked' : '' ?>>$751K - $950K</label>
			<label for="951K">
				<input 
					type="checkbox" 
					id="951K" 
					name="agent_property_value" 
					value="$951K - $1.5M" 
					<?php echo ( isset( $_GET['agent_property_value'] ) &&  $_GET['agent_property_value'] === 'vacant_land' ) ? 'checked' : '' ?>>$951K - $1.5M</label>
			<label for="1.5M">
				<input 
					type="checkbox" 
					id="1.5M" 
					name="agent_property_value" 
					value="$1.5M – $3M" 
					<?php echo ( isset( $_GET['agent_property_value'] ) &&  $_GET['agent_property_value'] === 'vacant_land' ) ? 'checked' : '' ?>>$1.5M – $3M</label>
			<label for="3M">
				<input 
					type="checkbox" 
					id="3M" 
					name="agent_property_value" 
					value="$3M and more" 
					<?php echo ( isset( $_GET['agent_property_value'] ) &&  $_GET['agent_property_value'] === 'vacant_land' ) ? 'checked' : '' ?>>$3M and more</label>
		</div>

		<div class="fields-wrap one-col">
			<h4>Zip Code of property to Sell, or desired area to Purchase:</h4>
			<input type="text" name="zipcode" value="<?php echo ( isset( $_GET['zipcode'] ) ) ? $_GET['zipcode'] : '' ?>">
		</div>

		<div class="fields-wrap one-col">
			<h4>Language:</h4>
			<select name="language" id="language">
				<option value="No">No</option>
				<option value="English" <?php echo ( isset( $_GET['language'] ) &&  $_GET['language'] === 'English' ) ? 'selected' : '' ?>>English</option>
				<?php foreach ( agents_backend_get_languages() as $language ): ?>
					<option value="<?php echo $language ?>" <?php echo ( isset( $_GET['language'] ) &&  $_GET['language'] === $language ) ? 'selected' : '' ?>><?php echo $language ?></option>
				<?php endforeach ?>
			</select>
		</div>

		<div class="fields-wrap one-col">
			<h4>Enter agent name, email or company:</h4>
			<input type="text" name="word" value="<?php echo ( isset( $_GET['word'] ) ) ? $_GET['word'] : '' ?>" placeholder="agent name, email or company...">
		</div>

		<div class="btn-wrap one-col">
			<button class="btn primary">Filter</button>
			<button id="clear" class="btn btn-secondary">Clear Search Criteria</button>
		</div>
	</form>
</div>