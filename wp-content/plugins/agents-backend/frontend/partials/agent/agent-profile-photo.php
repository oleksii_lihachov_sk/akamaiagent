<?php if ( $agent->ID === get_current_user_id() ): ?>
	<div class="profile-status">
		<p class="<?php the_field( 'agent_status', 'user_' . $agent->ID ); ?>"><?php echo ucfirst( get_field( 'agent_status', 'user_' . $agent->ID ) ); ?></p>
	</div>
<?php endif ?>

<div class="photo">
	<img src="<?php echo $photo['sizes']['agent-photo']; ?>" alt="">
</div>