<?php

namespace Agents_Backend;

/**
 * The code used on the frontend.
 */
class Frontend {

	private $plugin_slug;
	private $version;
	private $option_name;
	private $settings;

	public function __construct( $plugin_slug, $version, $option_name ) {
		$this->plugin_slug = $plugin_slug;
		$this->version     = $version;
		$this->option_name = $option_name;
		$this->settings    = get_option( $this->option_name );
	}

	public function assets() {
		wp_enqueue_style( $this->plugin_slug, plugin_dir_url( __FILE__ ) . 'css/agents-backend-frontend.css', array(), $this->version );

		wp_enqueue_script( $this->plugin_slug, plugin_dir_url( __FILE__ ) . 'js/agents-backend-frontend.js', array( 'jquery' ), time(), true );

		wp_localize_script(
			$this->plugin_slug,
			'ajax',
			array(
				'ajax_url'   => admin_url( 'admin-ajax.php' ),
				'user_id'    => get_current_user_id(),
				'ajax_nonce' => wp_create_nonce( 'agents_nonce' ),
			)
		);
	}


	/**
	 * Adding ACF Form head
	 */
	public function add_acf_form_head() {
		acf_form_head();
	}


	/**
	 * Redirects
	 *
	 * @param int $id  Application ID
	 */
	public function users_redirects() {

		$action = ( isset( $_POST['action'] ) ) ? $_POST['action'] : '';

		$user = wp_get_current_user();

		if ( ! $user || is_wp_error( $user ) ) {
			return;
		}

		// Redirect Agents
		// if ( in_array( 'agent', (array) $user->roles ) ) {
		// $terms_status = get_field( 'agent_accepted_terms', 'user_' . $user->ID );
		//
		// if ( 'yes' !== $terms_status && ! is_page( 107 ) ) {
		// wp_redirect( get_permalink( 107 ), 302 );
		// exit;
		// }
		// }

		// Redirect not Agents
		if ( ! in_array( 'agent', (array) $user->roles ) && ! current_user_can( 'manage_options' ) ) {
			if ( is_page( 'agent-dashboard' ) || is_page( 'agent-account' ) ) {
				wp_redirect( home_url(), 302 );
				exit;
			}
		}

		// Redirect not Admins
		if ( ! current_user_can( 'manage_options' ) ) {

			if ( is_page( 'admin-agents-applications' ) ) {
				wp_redirect( home_url(), 302 );
				exit;
			}

			if ( is_page( 'admin-agents' ) ) {
				wp_redirect( home_url(), 302 );
				exit;
			}

			if ( is_page( 'admin-customers-requests' ) ) {
				wp_redirect( home_url(), 302 );
				exit;
			}

			if ( is_page( 'admin-email-center' ) ) {
				wp_redirect( home_url(), 302 );
				exit;
			}

			if ( is_page( 'admin-account' ) ) {
				wp_redirect( home_url(), 302 );
				exit;
			}
		}

		// Email center redirects
		if ( $action === 'admin-message' && current_user_can( 'manage_options' ) ) {
			$this->process_emails_center( $action );
		}

	}

	public function login_redirect( $redirect_to, $request, $user ) {
		// is there a user to check?
		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
			// check for admins
			if ( in_array( 'administrator', $user->roles ) ) {
				// redirect them to the default place
				return site_url( 'admin-dashboard' );
			} elseif ( in_array( 'agent', $user->roles ) ) {
				// redirect them to the default place
				return site_url( 'agent-dashboard' );
			} else {
				return home_url();
			}
		} else {
			return $redirect_to;
		}
	}

	/**
	 * Render Agents Applications shortcode
	 *
	 * @return string
	 */
	public function render_agents_applications() {

		if ( ! current_user_can( 'manage_options' ) ) {
			return false;
		}

		$action = ( isset( $_GET['action'] ) ) ? $_GET['action'] : '';

		$applications = get_posts(
			array(
				'post_type'   => 'agent_application',
				'numberposts' => -1,
				'meta_key'    => 'application_status',
			// 'meta_value'    => 'New'
			)
		);

		ob_start();

		switch ( $action ) {
			case 'view':
				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-application-view.php';
				break;

			case 'approve':
				if ( isset( $_GET['application_id'] ) ) {
					$result = Agent::approve_application( $_GET['application_id'] );
				}
				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agents-applications.php';
				break;

			case 'reject':
				if ( isset( $_GET['application_id'] ) ) {
					$result = Agent::reject_application( $_GET['application_id'] );
				}
				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agents-applications.php';
				break;

			case 'register':
				if ( isset( $_GET['application_id'] ) ) {
					$result = Agent::register_profile( $_GET['application_id'] );
				}
				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agents-applications.php';
				break;

			default:
				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agents-applications.php';
				break;
		}

		$html = ob_get_contents();
		ob_clean();

		return $html;

	}


	public function render_agents_search_tool() {

		if ( ! current_user_can( 'manage_options' ) ) {
			return false;
		}

		$action = ( isset( $_GET['action'] ) ) ? $_GET['action'] : '';

		ob_start();

		switch ( $action ) {
			case 'view':
				$agent = get_user_by( 'id', $_GET['agent_id'] );

				$photo           = get_field( 'agent_photo', 'user_' . $agent->ID );
				$company_address = get_field( 'agent_company_address', 'user_' . $agent->ID );
				$testimonials    = get_field( 'agent_testimonials', 'user_' . $agent->ID );

				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-profile.php';
				break;

			default:
				$agent_query = \Agents_Backend\Agent::get_agents();

				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agents-search-tool.php';
				break;
		}

		$html = ob_get_contents();
		ob_clean();

		return $html;
	}


	/**
	 * Render Agent Profile shortcode
	 *
	 * @return string
	 */
	public function render_agent_profile() {

		if (
			( isset( $_GET['agent_id'] ) &&
			  isset( $_GET['customer_request_id'] )
			) ||
			( isset( $_GET['agent_id'] ) &&
			  current_user_can( 'manage_options' ) )
		) {

			if ( current_user_can( 'manage_options' ) ) {
				$agent = get_user_by( 'id', $_GET['agent_id'] );
			} else {
				 $assigned_agents = get_field( 'customer_request_assigned_agents', $_GET['customer_request_id'] );

				if ( is_array( $assigned_agents ) && in_array( $_GET['agent_id'], $assigned_agents ) ) {
					$agent = get_user_by( 'id', $_GET['agent_id'] );
				} else {
					$agent = false;
				}
			}
		} elseif ( ! is_user_logged_in() ) {
			$agent = false;
		} else {
			$agent = wp_get_current_user();
		}

		if ( ! $agent ) {
			ob_start();
			include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-profile-no-access.php';
			$html = ob_get_contents();
			ob_clean();

			return $html;
		}

		if ( isset( $_GET['edit'] ) && ( $agent->ID == get_current_user_id() || current_user_can( 'manage_options' ) ) ) {
			ob_start();
			include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-profile-edit.php';
			$html = ob_get_contents();
			ob_clean();

			return $html;
		}

		$agent_class = new Agent( $agent->ID );

		$photo           = get_field( 'agent_photo', 'user_' . $agent->ID );
		$company_address = get_field( 'agent_company_address', 'user_' . $agent->ID );
		$testimonials    = get_field( 'agent_testimonials', 'user_' . $agent->ID );

		ob_start();
		include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-profile.php';

		$html = ob_get_contents();
		ob_clean();

		return $html;
	}


	/**
	 * Render Account Page shortcode
	 *
	 * @return string
	 */
	public function render_agent_account() {

		$current_user = wp_get_current_user();
		$user_class   = new Agent( $current_user->ID );

		$error = false;

		if ( isset( $_POST['action'] ) &&
			'update-account' === $_POST['action'] &&
			! is_admin() &&
			$current_user->ID === intval( $_POST['user_id'] )
		) {
			if ( wp_verify_nonce( $_POST['update-account-nonce'], 'update_account_nonce' ) ) {

				if ( isset( $_POST['updateuser'] ) ) {
					$error = $user_class->update_agent_account();
				}
			} else {
				$error[] = __( 'Permission Denied', 'agents-backend' );
			}
		}

		ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/agent/agent-account-edit.php';
		$html = ob_get_contents();
		ob_clean();

		return $html;

	}


	/**
	 * Render Customer Requests shortcode
	 *
	 * @return string
	 */
	public function render_customer_requests() {

		if ( ! current_user_can( 'manage_options' ) ) {
			return false;
		}

		$action = ( isset( $_GET['action'] ) ) ? $_GET['action'] : '';

		$requests = \Agents_Backend\Request::get_customer_requests();

		ob_start();

		switch ( $action ) {
			case 'view':
				$request_id    = ( isset( $_GET['request_id'] ) ) ? $_GET['request_id'] : null;
				$request_class = new \Agents_Backend\Request( $request_id );

				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/customer/customer-request-view.php';
				break;

			default:
				include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/customer/customer-requests.php';
				break;
		}

		$html = ob_get_contents();
		ob_clean();

		return $html;
	}

	/**
	 * Render Account Page shortcode
	 *
	 * @return string
	 */
	public function render_admin_account() {

		if ( ! current_user_can( 'manage_options' ) ) {
			return false;
		}

		$current_user = wp_get_current_user();
		$user_class   = new Agent( $current_user->ID );

		$error = false;

		if ( isset( $_POST['action'] ) &&
			'update-account' === $_POST['action'] &&
			! is_admin() &&
			$current_user->ID === intval( $_POST['user_id'] )
		) {
			if ( wp_verify_nonce( $_POST['update-account-nonce'], 'update_account_nonce' ) ) {

				$error      = $user_class->update_agent_account();
				$user_class = new Agent( $current_user->ID );

			} else {
				$error[] = __( 'Permission Denied', 'agents-backend' );
			}
		}

		// ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/admin/admin-account-edit.php';
		// $html = ob_get_contents();
		// ob_clean();

		// return $html;
	}

	/**
	 * Render Customer Requests shortcode
	 *
	 * @return string
	 */
	public function render_email_center() {

		if ( ! current_user_can( 'manage_options' ) ) {
			return false;
		}

		$recipients = get_all_recipients();

		$templates = get_posts(
			array(
				'post_type'   => 'template',
				'numberposts' => -1,
			)
		);

		ob_start();

		include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/email-center/email-center.php';

		$html = ob_get_contents();
		ob_clean();

		return $html;
	}

	private function process_emails_center( $action ) {
		switch ( $action ) {
			case 'admin-message':
				if ( isset( $_POST['recipient'] ) && $_POST['recipient'] === 'agents' ) {
					$agents = get_users(
						array(
							'number'     => -1,
							'role'       => 'agent',
							'meta_key'   => 'agent_subscription',
							'meta_value' => true,
						)
					);

					$result = Email::send_to_many(
						$agents,
						'agents',
						stripslashes( sanitize_text_field( $_POST['subject'] ) ),
						nl2br( stripslashes( $_POST['admin_message'] ) )
					);
				} elseif ( isset( $_POST['recipient'] ) && $_POST['recipient'] === 'user' ) {
					$user = get_user_by( 'email', $_POST['user-email'] );

					if ( ! $user ) {
						return;
					}

					$result = Email::send_email(
						'any-user',
						array(
							'to'      => sanitize_text_field( $_POST['user-email'] ),
							'subject' => stripslashes( sanitize_text_field( $_POST['subject'] ) ) . ' - ' . $user->first_name . ' ' . $user->last_name,
							'message' => nl2br( stripslashes( $_POST['admin_message'] ) ),
						)
					);
				} elseif ( isset( $_POST['recipient'] ) && $_POST['recipient'] === 'everyone' ) {
					$recipients = get_all_recipients_detailed();

					$result = Email::send_to_many(
						$recipients,
						'everyone',
						stripslashes( sanitize_text_field( $_POST['subject'] ) ),
						nl2br( stripslashes( $_POST['admin_message'] ) )
					);
				} elseif ( isset( $_POST['recipient'] ) && $_POST['recipient'] === 'customers' ) {
					$recipients = get_customer_recipients_detailed();
					$result     = Email::send_to_many(
						$recipients,
						'customers',
						stripslashes( sanitize_text_field( $_POST['subject'] ) ),
						nl2br( stripslashes( $_POST['admin_message'] ) )
					);
				}

				wp_safe_redirect(
					esc_url_raw(
						add_query_arg(
							array(
								'status'  => 'success',
								'message' => 'Message sent',
							),
							'/admin-email-center/'
						)
					)
				);
				die();

			default:
				break;
		}
	}

	public function show_popup_messages() {
		if ( isset( $_GET['action'] ) && $_GET['action'] === 'email_confirmed' ) {
			include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/notifications/confirmed-email.php';
		}

		if ( isset( $_GET['action'] ) && $_GET['action'] === 'email_confirmation_wrong' ) {
			include plugin_dir_path( dirname( __FILE__ ) ) . 'frontend/partials/notifications/email-confirmation-wrong-url.php';
		}
	}

	public function contact_us_email_sent( $form ) {
		if ( $form->id() === 1802 ) {
			$twilio = new TwilioService();
			$twilio->send_sms( 'contact' );
		}
	}
}
