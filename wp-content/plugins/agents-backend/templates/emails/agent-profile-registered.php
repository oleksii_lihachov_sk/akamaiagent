

<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<p>Congratulations, <?php echo $params['first_name']; ?>! We now have a signed referral agreement!</p>
				<p>Now, lets get you registered so you can start receiving referrals.</p>
				<p>Your user name is <?php echo $params['to']; ?>.</p>
				<p>Please set your password <a href="<?php echo $rp_link; ?>">here</a>.</p>
				<p>Once you have registered your account, you will automatically be logged into your Agent Center, where you may view/edit your account information, turn on/off your vacation status, and view/edit your profile page.</p>
				<p>Your profile page is what we will send to customers, so I recommend that you make it compelling. Also, you will not be entered into the agent search results until your profile page is COMPLETELY filled out.</p>
				<p>Have any questions? Please feel free to check out our <a href="http://akamaiagent.com/agent-faq/">Agent FAQ</a> section, reply to this email or give me a call.</p>
			</td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>