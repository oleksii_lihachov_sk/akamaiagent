<?php
    $type = $params['type'];
    $type_slug = strtolower($type);
    $relust = $params['result'];

    $first_name = $data->customer_first_name;
    $last_name = $data->customer_last_name;
?>

	<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
		<tbody>
		<tr>
			<td>
				<p>Aloha, Nick!</p>
				<p>You have received a Customer Request!  ☺</p>
				<p>Go to the <a href="http://akamaiagent.com/admin-customers-requests/?action=view&request_id=<?php echo $data->request_custom_id; ?>">search tool</a> to select and refer appropriate agents NOW!!</p>
				<p>Make it happen, Niko!</p>
                
                <h3>Customer Request:</h3>
                <table class="table-data">
                    <tr>
                        <td>Request ID</td>
                        <td><?php echo $data->request_custom_id; ?></td>
                    </tr>
                    <tr>
                        <td><strong>First Name</strong></td>
                        <td><?php echo $data->customer_first_name; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Last Name</strong></td>
                        <td><?php echo $data->customer_last_name; ?></td>
                    </tr>
                    <tr>
                        <td><strong>Email</strong></td>
                        <td><?php echo $data->customer_email; ?></td>
                    </tr>
                    <?php 

                        if ( 'Seller & Buyer' === $type ) {
                            $types = [ 'Seller', 'Buyer' ];
                        } else {
                            $types = [ $type ];
                        }

                    ?>
                    <?php foreach ( $types as $key => $single_type ): ?>
                        <tr>
                            <td><strong>What do you need an agent for?</strong></td>
                            <td>
                                <?php if( 'Seller' === $single_type ){
                                    echo 'Selling a property';
                                } else {
                                    echo 'Buying a property';
                                }?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Type of Property</strong></td>
                            <td><?php echo $data->$type_slug['property_type']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Estimated Property Value</strong></td>
                            <td><?php echo $data->$type_slug['price_range']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Your Timeline</strong></td>
                            <td><?php echo $data->$type_slug['how_soon']; ?></td>
                        </tr>
                        <tr>
                            <td><strong>Agent Language Preference</strong></td>
                            <td>
                                <?php echo $data->other_language; ?>
                            </td>
                        </tr>
                        
                        <?php if( 'Seller' === $single_type ) : ?>
                            <tr>
                                <td colspan="2"><strong>Property Address:</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Street</strong></td>
                                <td><?php echo $data->seller['address']['street']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Additional Street Address</strong></td>
                                <td><?php echo $data->seller['address']['unit']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>City</strong></td>
                                <td><?php echo $data->seller['address']['city']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>State</strong></td>
                                <td><?php echo $data->seller['address']['state']; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Zip Code</strong></td>
                                <td><?php echo $data->seller['address']['zipcode']; ?></td>
                            </tr>
                        <?php endif; ?>

                        <?php if( 'Buyer' === $single_type ) : ?>
                            <tr>
                                <td colspan="2">Property Search Area</strong></td>
                            </tr>
                            <tr>
                                <td><strong>Zip Code</strong></td>
                                <td><?php echo $data->buyer['address']['zipcode']; ?></td>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach ?>
                    
                </table>
			</td>
		</tr>
		</tbody>
	</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>