<table role="presentation" cellpadding="0" border="0" cellpadding="0" cellspacing="0" width="100%" class="agents-table">
	<?php foreach( $params['agents'] as $agent_id ) : ?>
		<?php
			$agent = get_user_by( 'ID', $agent_id );
			$photo = get_field( 'agent_photo', 'user_' . $agent_id );
		?>

		<tr>
			<td class="column" width="75" headers="75">
                <!--[if mso]>
                <table width="50%"><tr><td><img width="75" src="<?php echo $photo['sizes']['agent-photo']; ?>" alt="" style="display:inline-block;width: 75px;height: 75px;max-width: 75px;max-height: 75px;margin: auto;overflow: hidden;border-radius: 50%;"></td></tr></table>
                <div style="display:none">
                <![endif]-->
                <span style="display:inline-block;width: 75px;height: 75px;max-width: 75px;max-height: 75px;margin: auto;overflow: hidden;border-radius: 50%;">
                    <img style="width: 100%;" src="<?php echo $photo['sizes']['agent-photo']; ?>" alt="">
                </span>
                <!--[if mso]>
                </div>
                <![endif]-->
            </td>
			<td class="column">
				<p style="margin: 0;font-weight: 600;"><?php echo $agent->first_name; ?> <?php echo $agent->last_name; ?></p>
				<p style="margin: 0;"><?php the_field( 'agent_company_name', 'user_' . $agent_id ); ?></p>
			</td>
			<td class="column" style="text-align: center">
				<p style="margin: 0;color: #6099C1;font-size: 20px;"><b><?php the_field( 'agent_transactions_in_last_year', 'user_' . $agent_id ); ?></b></p>
				<p>Number of Transactions<br>in the last 12 months</p>
			</td>
			<td class="column last" style="width: 120px;text-align:center">
                <a style="text-decoration: none; background: #FF9948; background-color: #FF9948; display:inline-block; padding: 7px; border-radius: 5px; color: white; font-size: 13px; font-weight: 600;" href="http://akamaiagent.com/agent-profile/?customer_request_id=<?php echo $request_id; ?>&agent_id=<?php echo $agent_id; ?>"><span style="">View Profile</span></a>
            </td>
		</tr>
	<?php endforeach; ?>
</table>