<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<p>Aloha, <?php echo $params['first_name']; ?>!</p>
				
				<div>
					<?php echo $params['message']; ?>
				</div>

				<p>Feel free to <a href="http://akamaiagent.com/contact-us/">Contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our Akamai Agents! (Please note our hours of operation) </p>
			</td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>