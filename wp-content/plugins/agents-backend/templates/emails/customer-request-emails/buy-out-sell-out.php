<p>Aloha, <?php echo $first_name; ?>!</p>

<p>Thank you for trusting us to find the perfect agent(s) to assist you with your real estate needs!</p>

<p>As requested, we will reach out to our contacts regarding your planned sale and purchase outside of Hawaii, and we’ll let you know as soon as we single out a top area expert(s) to assist you with those transactions.</p>

<p>We make every effort to align you with Hawaii’s very best agents, based on your individual real estate goals, although you are never under any obligation work with an agent we refer.</p>

<p>Please feel free to <a href="http://akamaiagent.com/contact-us/">Contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our wonderful customers!</p>

<p>You may also take a look at our frequently asked questions (FAQ) <a href="http://akamaiagent.com/customer-faq/">here</a></p>