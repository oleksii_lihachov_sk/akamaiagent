<p>Aloha, <?php echo $first_name; ?>!</p>

<p>Thank you for trusting us to find the perfect agent to assist you with your Hawaii property purchase!</p>

<p>We have received your request, and are working to compile your top Hawaii agent profiles now. We will get them off to you shortly (please note our hours of operation below).</p>

<p>We make every effort to align you with the very best agents, based on your individual real estate goals, although you are never under any obligation work with an agent we refer.</p>

<p>Please feel free to <a href="http://akamaiagent.com/contact-us/">Contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our wonderful customers!</p>

<p>You may also take a look at our frequently asked questions (FAQ) <a href="http://akamaiagent.com/customer-faq/">here</a></p>

