<?php
// @codingStandardsIgnoreFile
	$type = $params['type'];
    $type_slug = strtolower($type);
	$relust = $params['result'];

	$first_name = $data->customer_first_name;
	$last_name = $data->customer_last_name;

    // Buy IN
    if ( $type === 'Buyer' && ( $relust === 'GN-1' || $relust === 'SN-2' || $relust === 'GN-4' ) ){
        $meta_query = [
            [
                'key'   => 'system_email_type',
                'value' => 'buy-in'
            ]
        ];
    }

    // Sell IN
    if ( ( $type === 'Seller' && ( $relust === 'GN-1' || $relust === 'BN-2' || $relust === 'GN-5' ) ) ) {
        $meta_query = [
            [
                'key'   => 'system_email_type',
                'value' => 'sell-in'
            ]
        ];
    }

    // Buy OUT
    if ( $type === 'Buyer' && ( $relust === 'BN-2' || $relust === 'GN-3' || $relust === 'BN-2' ) ) {
        $meta_query = [
            [
                'key'   => 'system_email_type',
                'value' => 'buy-out'
            ]
        ];
    }

    // Sell OUT
    if ( $type === 'Seller' && ( $relust === 'SN-2' || $relust === 'GN-3' || $relust === 'SN-2' ) ) {
        $meta_query = [
            [
                'key'   => 'system_email_type',
                'value' => 'sell-out'
            ]
        ];
    }

    $email_template = get_posts([
        'post_type'   => 'system-email',
        'numberposts' => 1,
        'meta_query'  => $meta_query
    ]);
?>

<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
	<tr>
		<td>
            <?php
                if( is_array( $email_template ) && count( $email_template ) > 0 ) {
                    echo str_replace( '{first_name}', $first_name, wpautop($email_template[0]->post_content) );
                }
            ?>
        </td>
	</tr>
	<tr>
		<h3>Customer Information:</h3>
		<table class="table-data">
            <tr>
                <td>First Name</td>
                <td><?php echo $first_name; ?></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><?php echo $last_name; ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?php echo $data->customer_email; ?></td>
            </tr>
            <?php 

                if ( 'Seller & Buyer' === $type ) {
                    $types = [ 'seller', 'buyer' ];
                } else {
                    $types = [ $type_slug ];
                }

            ?>
            <?php foreach ( $types as $key => $single_type ): ?>
                <tr>
                    <td>What do you need an agent for?</td>
                    <td>
                        <?php if( 'seller' === $single_type ){
                            echo 'Selling a property';
                        } else {
                            echo 'Buying a property';
                        }?>
                    </td>
                </tr>
                <tr>
                    <td>Type of Property</td>
                    <td><?php echo $data->$single_type['property_type']; ?></td>
                </tr>
                <tr>
                    <td>Estimated Property Value</td>
                    <td><?php echo $data->$single_type['price_range']; ?></td>
                </tr>
                <tr>
                    <td>Your Timeline</td>
                    <td><?php echo $data->$single_type['how_soon']; ?></td>
                </tr>
                <tr>
                    <td>Agent Language Preference</td>
                    <td>
                        <?php echo $data->other_language; ?>
                    </td>
                </tr>
                
                <?php if( 'seller' === $single_type ) : ?>
                    <tr>
                        <td colspan="2"><strong>Property Address:</strong></td>
                    </tr>
                    <tr>
                        <td>Street</td>
                        <td><?php echo $data->seller['address']['street']; ?></td>
                    </tr>
                    <tr>
                        <td>Additional Street Address</td>
                        <td><?php echo $data->seller['address']['unit']; ?></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td><?php echo $data->seller['address']['city']; ?></td>
                    </tr>
                    <tr>
                        <td>State</td>
                        <td><?php echo $data->seller['address']['state']; ?></td>
                    </tr>
                    <tr>
                        <td>Zip Code</td>
                        <td><?php echo $data->seller['address']['zipcode']; ?></td>
                    </tr>
                <?php endif; ?>

                <?php if( 'buyer' === $single_type ) : ?>
                    <tr>
                        <td colspan="2"><strong>Property Search Area</strong></td>
                    </tr>
                    <tr>
                        <td>Zip Code</td>
                        <td><?php echo $data->buyer['address']['zipcode']; ?></td>
                    </tr>
                <?php endif; ?>
            <?php endforeach ?>
            
        </table>
	</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __DIR__ ) ) . 'emails/email-contacts.php'; ?>