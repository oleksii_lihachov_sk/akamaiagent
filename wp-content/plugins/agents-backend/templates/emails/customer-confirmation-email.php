<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
    	<tr>
    		<td>
    			<p>Hi, <?php echo $params['first_name']; ?>!</p>
                <p>Confirm your email address by clicking below, and we’ll get your Akamai Agent profiles off to you shortly.</p>
                <p><a 
                        style="width: 100px;
                               display: block;
                               margin: auto;
                               text-align: center;
                               background: #FF9948;
                               background-color: #FF9948;
                               color: #fff;
                               text-decoration: none;
                               font-size: 18px;
                               padding: 7px;
                               border-radius: 5px;" 
                        href="<?php echo site_url(); ?>?action=confirm_email&request_id=<?php echo $params['request_id']; ?>&confirmation_token=<?php echo $params['confirmation_token']; ?>"
                    >Confirm</a>
                </p>
            </td>
    	</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>