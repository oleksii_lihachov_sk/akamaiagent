<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<p>Thank you for showing an interest in becoming an Akamai Agent, <?php echo $params['first_name']; ?>!</p>
				<p>Your request has been received. I will review your information and get back to you shortly.</p>
				<p>If you have any questions, please see the <a href="http://akamaiagent.com/agent-faq/">Agent FAQ section</a>, give me a call, or simply respond to this email.</p>
			    <p>Please  note our hours of operation below.</p>
            </td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>

<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<p>Your responses:</p>
				<table class="table-data">
					<tbody>
						<?php foreach ( $data as $key => $value ): ?>
							<tr>
								<td><?php echo $key ?></td>
								<td><?php echo $value ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>