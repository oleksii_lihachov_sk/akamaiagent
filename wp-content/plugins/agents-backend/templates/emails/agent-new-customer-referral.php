<?php

/**
 * @var \Agents_Backend\Request $data Request class
 */

$request_id = $params['customer_request_id'];

$first_name = $data->customer_first_name;
$last_name  = $data->customer_last_name;

$type = $data->request_type;

?>

<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
	<tr>
		<td>
			<p>Congratulations, <?php echo $params['agent_first_name']; ?>!</p>
			<p>We have shared your profile with a customer for consideration (see customer information below). Wishing you the best of luck!</p>
			<p>Note: It is very important that you:</p>
			<table role="presentation" cellpadding="0" border="0" cellpadding="0" cellspacing="0" width="100%" class="list">
                <tr>
                    <td><span style="padding-right: 10px">-</span></td>
					<td>Notify us immediately upon first contact with the referred customer.</td>
                </tr>
                <tr>
                    <td><span style="padding-right: 10px">-</span></td>
					<td>Keep us in the loop throughout the transaction process.</td>
                </tr>
			</table>
			<p>We do not share customer contact information with agents, although the customer has been given your contact information.</p>
			<p>We’re always happy to hear from you should you have any questions or comments.<br>
				You may contact us <a href="http://akamaiagent.com/contact-us/">here</a>.<br>
				(Please note our hours of operation below).</p>

			<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/customer-no-contact-request-table.php'; ?>

		</td>
	</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>