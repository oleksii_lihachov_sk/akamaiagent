<table style="border-bottom: 1px solid #ccc;padding-bottom:20px;margin-bottom:20px;">
	<tbody>
		<tr>
			<td>
				<?php if ( $params['vacation_status'] === 'yes' ): ?>
					
					<p>Aloha, <?php echo $params['first_name']; ?>!</p>
	 
					<p>We noticed that you turned on your Vacation Status.</p>
					 
					<p>You will not be referred to customers while your Vacation Status is turned on.</p>
					 
					<p>If you did this by mistake, please see the Vacation Status option in your Dashboard.</p>
					 
					<p>Feel free to <a href="http://akamaiagent.com/contact-us/">Contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our Akamai Agents! (Please note our hours of operation)</p>
					 
					<p>Please remember to turn your Vacation Status off once you’re ready to start receiving customer referrals again.</p>
					 
					<p>Enjoy your vacation!</p>
				
				<?php else : ?>

					<p>Aloha, <?php echo $params['first_name']; ?>!</p>
	 
					<p>We noticed that you turned off your Vacation Status.  Welcome back!</p>
					 
					<p>You will now start being referred to customers once again. We’ll be sure to let you know when we refer you to an appropriate customer.</p>
					 
					<p>Feel free to <a href="http://akamaiagent.com/contact-us/">Contact us</a> if you have any questions or comments. It’s always a pleasure to hear from our Akamai Agents! (Please note our hours of operation)</p>

				<?php endif; ?>
			</td>
		</tr>
	</tbody>
</table>

<?php include plugin_dir_path( dirname( __FILE__ ) ) . 'emails/email-contacts.php'; ?>