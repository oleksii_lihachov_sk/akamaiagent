<?php if( 'Seller' === $type || 'Seller & Buyer' === $type ): ?>
    <h3 style="margin-top: 50px;">Seller Criteria:</h3>
    <table class="table-data">
        <tr>
            <td>First Name</td>
            <td><?php echo $data->customer_first_name; ?></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><?php echo $data->customer_last_name; ?></td>
        </tr>
        <tr>
            <td>Type of Property</td>
            <td><?php echo $data->seller['property_type']; ?></td>
        </tr>
        <tr>
            <td>Estimated Property Value</td>
            <td><?php echo $data->seller['price_range']; ?></td>
        </tr>
        <tr>
            <td>Your Timeline</td>
            <td><?php echo $data->seller['how_soon']; ?></td>
        </tr>
        <tr>
            <td>Agent Language Preference</td>
            <td><?php echo $data->other_language; ?></td>
        </tr>
        <tr>
            <td colspan="2">Property Address:</td>
        </tr>
        <tr>
            <td>City</td>
            <td><?php echo $data->seller['address']['city']; ?></td>
        </tr>
        <tr>
            <td>State</td>
            <td><?php echo $data->seller['address']['state']; ?></td>
        </tr>
        <tr>
            <td>Zip Code</td>
            <td><?php echo $data->seller['address']['zipcode']; ?></td>
        </tr>
    </table>
<?php endif; ?>

<?php if( 'Buyer' === $type || 'Seller & Buyer' === $type ): ?>
    <h3 style="margin-top: 50px;">Buyer Criteria:</h3>
    <table class="table-data">
        <tr>
            <td>First Name</td>
            <td><?php echo $data->customer_first_name; ?></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><?php echo $data->customer_last_name; ?></td>
        </tr>
        <tr>
            <td>Property Type</td>
            <td><?php echo $data->buyer['property_type'] ?></td>
        </tr>
        <tr>
            <td>Estimated Property Value</td>
            <td><?php echo $data->buyer['price_range']; ?></td>
        </tr>
        <tr>
            <td>Your Timeline</td>
            <td><?php echo $data->buyer['how_soon']; ?></td>
        </tr>
        <tr>
            <td>Agent Language Preference</td>
            <td><?php echo $data->other_language; ?></td>
        </tr>
        <tr>
            <td>Preapproved by a Lender?</td>
            <td><?php
                $preapproved_by_lender = get_field( 'customer_request_buyer_customer_request_preapproved_by_lender', $data->request_id );
                echo ( isset($preapproved_by_lender[0]) ) ? $preapproved_by_lender[0] : 'No';
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">Property Address:</td>
        </tr>
        <tr>
            <td>Zip Code</td>
            <td><?php echo $data->buyer['address']['zipcode']; ?></td>
        </tr>
    </table>
<?php endif; ?>